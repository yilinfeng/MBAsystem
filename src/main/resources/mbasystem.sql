/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : mbasystem

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-12-19 11:45:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `department`
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `departmentId` bigint(20) NOT NULL AUTO_INCREMENT,
  `departmentName` varchar(255) DEFAULT NULL,
  `describe` varchar(255) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`departmentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------

-- ----------------------------
-- Table structure for `employee`
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `employeeId` bigint(20) NOT NULL AUTO_INCREMENT,
  `department1` bigint(20) DEFAULT NULL,
  `department2` bigint(20) DEFAULT NULL,
  `department3` bigint(20) DEFAULT NULL,
  `jopType` bigint(20) DEFAULT NULL,
  `jopName` bigint(20) DEFAULT NULL,
  `professionalRanks` varchar(255) DEFAULT NULL,
  `employeeName` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `homeplace` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `nation` varchar(255) DEFAULT NULL,
  `faith` varchar(255) DEFAULT NULL,
  `politicalStatus` varchar(255) DEFAULT NULL,
  `identityNumber` varchar(255) DEFAULT NULL,
  `socialSecurityNumber` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `educationalBackground` varchar(255) DEFAULT NULL,
  `majorIn` varchar(255) DEFAULT NULL,
  `salaryStandard` bigint(20) DEFAULT NULL,
  `bankOfDeposit` varchar(255) DEFAULT NULL,
  `bankAccount` varchar(255) DEFAULT NULL,
  `speciality` varchar(255) DEFAULT NULL,
  `hobby` varchar(255) DEFAULT NULL,
  `curriculumvitae` varchar(255) DEFAULT NULL,
  `familyInfo` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `operater` varchar(255) DEFAULT NULL,
  `registerTime` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `havarecheck` bit(1) DEFAULT NULL,
  `delete` bit(1) DEFAULT NULL,
  PRIMARY KEY (`employeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee
-- ----------------------------

-- ----------------------------
-- Table structure for `position`
-- ----------------------------
DROP TABLE IF EXISTS `position`;
CREATE TABLE `position` (
  `positionId` bigint(20) NOT NULL AUTO_INCREMENT,
  `positionName` varchar(255) DEFAULT NULL,
  `describe` varchar(255) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`positionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of position
-- ----------------------------

-- ----------------------------
-- Table structure for `privilege`
-- ----------------------------
DROP TABLE IF EXISTS `privilege`;
CREATE TABLE `privilege` (
  `privilegeId` bigint(20) NOT NULL AUTO_INCREMENT,
  `privilegeName` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`privilegeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of privilege
-- ----------------------------
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (101, '待办任务', '/snaker/task/active');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (102, '流程实例', '/snaker/flow/order');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (103, '历史任务', '/snaker/task/history');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (104, '流程定义', '/snaker/process/list');
INSERT INTO privilege(privilegeId, privilegeName, url) VALUES (105, '流程部署', '/snaker/process/deploy/**;/snaker/process/add/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (106, '委托授权', '/snaker/surrogate/list');

INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (301, '字典查询', '/config/dictionary');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (302, '字典查看', '/config/dictionary/view/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (303, '字典编辑', '/config/dictionary/update/**;/config/dictionary/save/**;/config/dictionary/add/**;/config/dictionary/edit/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (304, '字典删除', '/config/dictionary/delete/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (311, '表单查询', '/config/form');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (312, '表单查看', '/config/form/view/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (313, '表单编辑', '/config/form/update/**;/config/form/save/**;/config/form/add/**;/config/form/edit/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (314, '表单删除', '/config/form/delete/**');

INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (401, '用户查询', '/security/user');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (402, '部门查询', '/security/org');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (403, '角色查询', '/security/role');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (404, '权限查询', '/security/authority');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (405, '资源查询', '/security/reurl');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (406, '菜单查询', '/security/menu');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (411, '用户查看', '/security/user/view/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (412, '部门查看', '/security/org/view/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (413, '角色查看', '/security/role/view/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (414, '权限查看', '/security/authority/view/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (415, '资源查看', '/security/reurl/view/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (416, '菜单查看', '/security/menu/view/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (421, '用户删除', '/security/user/delete/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (422, '部门删除', '/security/org/delete/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (423, '角色删除', '/security/role/delete/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (424, '权限删除', '/security/authority/delete/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (425, '资源删除', '/security/reurl/delete/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (426, '菜单删除', '/security/menu/delete/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (431, '用户编辑', '/security/user/update/**;/security/user/save/**;/security/user/add/**;/security/user/edit/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (432, '部门编辑', '/security/org/update/**;/security/org/save/**;/security/org/add/**;/security/org/edit/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (433, '角色编辑', '/security/role/update/**;/security/role/save/**;/security/role/add/**;/security/role/edit/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (434, '权限编辑', '/security/authority/update/**;/security/authority/save/**;/security/authority/add/**;/security/authority/edit/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (435, '资源编辑', '/security/reurl/update/**;/security/reurl/save/**;/security/reurl/add/**;/security/reurl/edit/**');
INSERT INTO privilege (privilegeId, privilegeName, url) VALUES (436, '菜单编辑', '/security/menu/update/**;/security/menu/save/**;/security/menu/add/**;/security/menu/edit/**');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `roleId` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` (roleId, roleName) VALUES (1, '系统管理员');
INSERT INTO `role` (roleId, roleName) VALUES (2, '普通用户');
-- ----------------------------
-- Table structure for `roleprivilegemanage`
-- ----------------------------
DROP TABLE IF EXISTS `roleprivilegemanage`;
CREATE TABLE `roleprivilegemanage` (
  `rolePrivilegeManageId` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleId` bigint(20) DEFAULT NULL,
  `privilegeId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`rolePrivilegeManageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roleprivilegemanage
-- ----------------------------
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 101);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 102);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 103);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 104);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 105);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 106);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 301);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 302);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 303);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 304);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 311);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 312);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 313);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 314);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 401);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 402);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 403);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 404);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 405);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 406);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 411);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 412);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 413);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 414);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 415);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 416);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 421);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 422);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 423);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 424);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 425);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 426);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 431);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 432);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 433);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 434);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 435);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (1, 436);

INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 101);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 102);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 103);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 104);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 106);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 301);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 302);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 401);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 402);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 403);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 404);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 405);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 406);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 411);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 412);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 413);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 414);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 415);
INSERT INTO roleprivilegemanage (roleId, privilegeId) VALUES (2, 416);
-- ----------------------------
-- Table structure for `salaryitem`
-- ----------------------------
DROP TABLE IF EXISTS `salaryitem`;
CREATE TABLE `salaryitem` (
  `salaryItemId` bigint(20) NOT NULL AUTO_INCREMENT,
  `salaryItemName` varchar(255) DEFAULT NULL,
  `salaryType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`salaryItemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of salaryitem
-- ----------------------------

-- ----------------------------
-- Table structure for `salaryorder`
-- ----------------------------
DROP TABLE IF EXISTS `salaryorder`;
CREATE TABLE `salaryorder` (
  `salaryOrderId` bigint(20) NOT NULL AUTO_INCREMENT,
  `salaryOrderNo` varchar(255) DEFAULT NULL,
  `peopleNumber` varchar(255) DEFAULT NULL,
  `basePrice` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`salaryOrderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of salaryorder
-- ----------------------------

-- ----------------------------
-- Table structure for `salaryorderdetail`
-- ----------------------------
DROP TABLE IF EXISTS `salaryorderdetail`;
CREATE TABLE `salaryorderdetail` (
  `salaryOrderDetailId` bigint(20) NOT NULL AUTO_INCREMENT,
  `privilegeId` bigint(20) DEFAULT NULL,
  `privilegeName` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`salaryOrderDetailId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of salaryorderdetail
-- ----------------------------

-- ----------------------------
-- Table structure for `salarystander`
-- ----------------------------
DROP TABLE IF EXISTS `salarystander`;
CREATE TABLE `salarystander` (
  `salaryStanderId` bigint(20) NOT NULL AUTO_INCREMENT,
  `salaryStanderName` varchar(255) DEFAULT NULL,
  `planner` varchar(255) DEFAULT NULL,
  `operater` varchar(255) DEFAULT NULL,
  `registerTime` datetime DEFAULT NULL,
  `salaryItemList` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `rechecker` varchar(255) DEFAULT NULL,
  `recheckOpinion` varchar(255) DEFAULT NULL,
  `havaRecheck` bit(1) DEFAULT NULL,
  PRIMARY KEY (`salaryStanderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of salarystander
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` bigint(20) NOT NULL AUTO_INCREMENT,
  `employeeName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` (userId, employeeName, PASSWORD)
VALUES (1, '易临风', 'f9e1a0299c2570eb5942fbbda0b2a8ceb2ef9769');
INSERT INTO `user` (userId, employeeName, PASSWORD)
VALUES (2, '若', 'f9e1a0299c2570eb5942fbbda0b2a8ceb2ef9769');
INSERT INTO `user` (userId, employeeName, PASSWORD)
VALUES (3,'snaker', 'f9e1a0299c2570eb5942fbbda0b2a8ceb2ef9769');

-- ----------------------------
-- Table structure for `userrolemanage`
-- ----------------------------
DROP TABLE IF EXISTS `userrolemanage`;
CREATE TABLE `userrolemanage` (
  `userRoleManageId` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL,
  `roleId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`userRoleManageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userrolemanage
-- ----------------------------
INSERT INTO userrolemanage (userId, roleId) VALUES (1, 1);
INSERT INTO userrolemanage (userId, roleId) VALUES (2, 2);
INSERT INTO userrolemanage (userId, roleId) VALUES (3, 2);

select distinct p.privilegeName from user u 
left outer join useRroleManage ur on u.userId=ur.userId 
left outer join role r on ur.roleId=r.roleId
left join RolePrivilegemanage rp on r.roleId = rp.roleId
left join Privilege p on rp.privilegeId=p.privilegeId where u.userId=2

select e.department1,e.department2,e.department3,count(*),sum(s.price) from employee e 
left outer join salarystander s on e.salarystandard=s.salarystanderId where e.havarecheck=true and e.isDelete=false
group by e.department3 


select * from employee e 
left outer join salarystander s on e.salarystandard=s.salarystanderId 
group by e.department3 

select e.employeeId,e.employeeName,e.department1,e.department2,e.department3,s.salaryItemList from employee e 
left outer join salarystander s on e.salarystandard=s.salarystanderId 
where e.havarecheck=true and e.isDelete=false and e.department3=8
