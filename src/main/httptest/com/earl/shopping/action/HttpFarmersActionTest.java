package com.earl.shopping.action;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;

public class HttpFarmersActionTest extends HttpBaseActionTest {

	/**
	 * log4j实例对象.
	 */
	private static Logger logger = LogManager.getLogger(HttpFarmersActionTest.class
			.getName());
	
	@Test
	public void testGetFarmerByShop() {
		String targetURL = basePath
				+ "farmers_getFarmerByShop.action";

		PostMethod filePost = new PostMethod(targetURL);

		Part[] parts = { new StringPart("shopId", "5", "utf-8"),
		};
		String sendHttpRequest = sendHttpRequest(filePost, parts);
		logger.info(sendHttpRequest);
	}

}