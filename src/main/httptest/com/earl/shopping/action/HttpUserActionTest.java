package com.earl.shopping.action;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;

public class HttpUserActionTest extends HttpBaseActionTest{

	/**
	 * log4j实例对象.
	 */
	private static Logger logger = LogManager.getLogger(HttpUserActionTest.class
			.getName());
	
    @Test
	public void testDoLogin() {
		String targetURL = basePath
				+ "user_dologin.action";

		PostMethod filePost = new PostMethod(targetURL);

		Part[] parts = { 
				new StringPart("employeeName", "易临风", "utf-8")
				,new StringPart("password", "123", "utf-8")
		};
		String sendHttpRequest = sendHttpRequest(filePost, parts);
		logger.info(sendHttpRequest);
	}
    
    
    @Test
   	public void testUpdatePassword() {
   		String targetURL = basePath
   				+ "user_updatePassword.action";

   		PostMethod filePost = new PostMethod(targetURL);

   		Part[] parts = { 
   				new StringPart("userId", "14", "utf-8")
   				,new StringPart("password", "1234567", "utf-8")
   				,new StringPart("newPassword", "123456", "utf-8")
   		};
   		String sendHttpRequest = sendHttpRequest(filePost, parts);
   		logger.info(sendHttpRequest);
   	}
    @Test
    public void testGenera() {
    	String targetURL = basePath
    			+ "salaryorder_generateOrder.action";
    	
    	PostMethod filePost = new PostMethod(targetURL);
    	
    	Part[] parts = { 
    			new StringPart("userId", "14", "utf-8")
    			,new StringPart("password", "1234567", "utf-8")
    			,new StringPart("newPassword", "123456", "utf-8")
    	};
    	String sendHttpRequest = sendHttpRequest(filePost, parts);
    	logger.info(sendHttpRequest);
    }
    
}