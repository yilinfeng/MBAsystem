package com.earl.mbasystem.domain.userrolemapping;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
 @Service(value = "userRoleManageService")
public class UserRoleManageServiceImpl extends BaseServiceImpl<UserRoleManagePo> implements
		UserRoleManageService {

	@Resource(name = "userRoleManageDao")
	UserRoleManageDao userRoleManageDao;

	@PostConstruct
	public void initBaseDao(){
		baseDao = userRoleManageDao;
	}
	
}
