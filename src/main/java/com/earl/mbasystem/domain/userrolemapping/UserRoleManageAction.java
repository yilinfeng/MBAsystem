package com.earl.mbasystem.domain.userrolemapping;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.annotation.ReturnValue;
import com.earl.mbasystem.domain.base.BaseAction;
import com.earl.mbasystem.vo.DwzVo;

/**
 * 
 * 用途+action 如Demo+Action-->DemoAction
 * 
 * @author Administrator
 * 
 */
@Controller(value = "userRoleManageAction")
@Scope(value = "prototype")
public class UserRoleManageAction extends BaseAction<UserRoleManagePo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3293435262298029608L;

	protected Object result;

	@ReturnValue //返回实体对象，或者其他任意对象
	public Object getResultMessage() {
		return result;
	}

	// 下面填写业务逻辑

	/**
	 * 添加对象.
	 * @author 黄祥谦.
	 */
	public void addUserRoleManage() {
		userRoleManageServer.save(model);
	}
	
	public void deleteUserRole(){
		userRoleManageServer.deleteById(model.getUserRoleManageId());
		DwzVo dwzVo = new DwzVo();
		dwzVo.setMessage("删除成功");
		dwzVo.setStatusCode("200");
		result=dwzVo;
	}
	
}
