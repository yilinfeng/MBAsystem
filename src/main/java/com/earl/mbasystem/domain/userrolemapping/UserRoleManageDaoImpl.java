package com.earl.mbasystem.domain.userrolemapping;

import org.springframework.stereotype.Repository;

import com.earl.mbasystem.domain.base.BaseDaoImpl;


/**
 * 
 * 
 */
@Repository("userRoleManageDao")
public class UserRoleManageDaoImpl extends BaseDaoImpl<UserRoleManagePo> implements UserRoleManageDao {
	// property constants
	
}