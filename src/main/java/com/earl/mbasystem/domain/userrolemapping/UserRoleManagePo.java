package com.earl.mbasystem.domain.userrolemapping;


public class UserRoleManagePo{


	private Long userRoleManageId;
	
	/**
	 * 字段描述：Long 
	 * 字段类型：roleId  
	 */
	private Long roleId ;
	
	/**
	 * 字段描述：Long 
	 * 字段类型：userId  
	 */
	private Long userId ;
	
	public Long getUserRoleManageId() {
		return userRoleManageId;
	}
	public void setUserRoleManageId(Long userRoleManageId) {
		this.userRoleManageId = userRoleManageId;
	}
	public void setRoleId(Long roleId){
		this.roleId = roleId;
	}
	public Long getRoleId() {
		return this.roleId;
	}
	public void setUserId(Long userId){
		this.userId = userId;
	}
	public Long getUserId() {
		return this.userId;
	}
}
