package com.earl.mbasystem.domain.salaryorder;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.earl.mbasystem.domain.base.BaseDaoImpl;
import com.earl.mbasystem.domain.salaryorderdetail.SalaryOrderDetailPo;


/**
 * 
 * 
 */
@Repository("salaryOrderDao")
public class SalaryOrderDaoImpl extends BaseDaoImpl<SalaryOrderPo> implements SalaryOrderDao {

	@Override
	public List<SalaryOrderPo> generatOrder() {
		// TODO 未测试.
		
		//员工查找出基本薪水，基本薪水聚合，，人数聚合，员工根据部门进行聚合
		String sql = "select e.department1 as department1,e.department2 as department2,e.department3 as department3,count(*) ,sum(s.price) from employee e "
		+"left join salarystander s on e.salarystandard=s.salarystanderId where e.havarecheck=true and e.isDelete=false "
		+"group by e.department3";
		@SuppressWarnings("unchecked")
		List<Object[]> list = getCurrentSession().createSQLQuery(sql).list();
		ArrayList<SalaryOrderPo> arrayList = new ArrayList<SalaryOrderPo>();
		for (Object[] objects : list) {
			System.out.println(objects[0]);
			SalaryOrderPo salaryOrderPo = new SalaryOrderPo();
			salaryOrderPo.setDepartment1(((BigInteger)objects[0]).longValue());
			salaryOrderPo.setDepartment2(((BigInteger)objects[1]).longValue());
			salaryOrderPo.setDepartment3(((BigInteger)objects[2]).longValue());
			salaryOrderPo.setHaveRecheck(false);
			salaryOrderPo.setHaveDeal(false);
			salaryOrderPo.setPeopleNumber(((BigInteger)objects[3]).toString());
			salaryOrderPo.setBasePrice(((Double)objects[4]).toString());
			arrayList.add(salaryOrderPo);
		}
		return arrayList;
	}

	@Override
	public void saveOrder(SalaryOrderPo salaryOrderPo, List<SalaryOrderDetailPo> salaryOrderDetail) {
		// TODO 未测试.
		Long save = (Long) getCurrentSession().save(salaryOrderPo);
		for (SalaryOrderDetailPo salaryOrderDetailPo : salaryOrderDetail) {
			salaryOrderDetailPo.setSalaryOrderId(save);
			getCurrentSession().save(salaryOrderDetailPo);
		}
		
	}

	@Override
	public List<SalaryOrderPo> allUnDealSalaryOrder() {
		// TODO 未测试.
		String hql ="from SalaryOrderPo where haveDeal=false";
		@SuppressWarnings("unchecked")
		List<SalaryOrderPo> list = getCurrentSession().createQuery(hql).list();
		return list;
	}

	@Override
	public List<SalaryOrderPo> allUncheckSalaryOrder() {
		// TODO 未测试.
		String hql ="from SalaryOrderPo where haveRecheck=false";
		@SuppressWarnings("unchecked")
		List<SalaryOrderPo> list = getCurrentSession().createQuery(hql).list();
		return list;
	}

	@Override
	public List<SalaryOrderPo> allcheckSalaryOrder() {
		String hql ="from SalaryOrderPo where haveRecheck=true";
		@SuppressWarnings("unchecked")
		List<SalaryOrderPo> list = getCurrentSession().createQuery(hql).list();
		return list;
	}
}