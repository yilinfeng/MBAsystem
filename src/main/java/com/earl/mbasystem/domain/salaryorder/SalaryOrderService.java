package com.earl.mbasystem.domain.salaryorder;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseService;

public interface SalaryOrderService extends BaseService<SalaryOrderPo>{

	/**
	 * 系统自动生成薪酬订单.
	 * @author 黄祥谦.
	 */
	void generatOrder();

	/**
	 * 查找未处理订单.
	 * @author 黄祥谦.
	 * @return
	 */
	List<SalaryOrderPo> allUnDealSalaryOrder();

	/**
	 * 未复核薪酬单》
	 * @author 黄祥谦.
	 * @return
	 */
	List<SalaryOrderPo> allUncheckSalaryOrder();

	/**
	 * 复核完成的薪酬单.
	 * @author 黄祥谦.
	 * @return
	 */
	List<SalaryOrderPo> allcheckSalaryOrder();

	

	
	
	
}
