package com.earl.mbasystem.domain.salaryorder;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseDao;
import com.earl.mbasystem.domain.salaryorderdetail.SalaryOrderDetailPo;

public interface SalaryOrderDao extends BaseDao<SalaryOrderPo>{

	/**
	 * 生成订单.
	 * @author 黄祥谦.
	 * @return TODO
	 */
	List<SalaryOrderPo> generatOrder();

	/**
	 * 添加订单.
	 * @author 黄祥谦.
	 * @param salaryOrderPo
	 * @param salaryOrderDetail TODO
	 */
	void saveOrder(SalaryOrderPo salaryOrderPo, List<SalaryOrderDetailPo> salaryOrderDetail);

	List<SalaryOrderPo> allUnDealSalaryOrder();

	List<SalaryOrderPo> allUncheckSalaryOrder();

	List<SalaryOrderPo> allcheckSalaryOrder();

}
