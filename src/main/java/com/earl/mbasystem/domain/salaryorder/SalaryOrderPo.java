package com.earl.mbasystem.domain.salaryorder;


public class SalaryOrderPo{


	/**
	 * 字段描述：Long 
	 * 字段类型：salaryOrderId  
	 */
	private Long salaryOrderId ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：salaryOrderNo  
	 */
	private String salaryOrderNo ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：peopleNumber  
	 */
	private String peopleNumber ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：basePrice  
	 */
	private String basePrice ;
	
	private Long department1;
	
	private Long department2;
	
	private Long department3;
	
	/**
	 * 字段描述：String 
	 * 字段类型：department  
	 */
	
	private Boolean haveRecheck;
	
	private Boolean haveDeal;
	
	public Boolean getHaveDeal() {
		return haveDeal;
	}
	public Long getDepartment1() {
		return department1;
	}

	public void setDepartment1(Long department1) {
		this.department1 = department1;
	}

	public Long getDepartment2() {
		return department2;
	}
	public void setDepartment2(Long department2) {
		this.department2 = department2;
	}
	public Long getDepartment3() {
		return department3;
	}
	public void setDepartment3(Long department3) {
		this.department3 = department3;
	}
	public void setHaveDeal(Boolean haveDeal) {
		this.haveDeal = haveDeal;
	}
	public Boolean getHaveRecheck() {
		return haveRecheck;
	}
	public void setHaveRecheck(Boolean haveRecheck) {
		this.haveRecheck = haveRecheck;
	}
	public void setSalaryOrderId(Long salaryOrderId){
		this.salaryOrderId = salaryOrderId;
	}
	public Long getSalaryOrderId() {
		return this.salaryOrderId;
	}
	public void setSalaryOrderNo(String salaryOrderNo){
		this.salaryOrderNo = salaryOrderNo;
	}
	public String getSalaryOrderNo() {
		return this.salaryOrderNo;
	}
	public void setPeopleNumber(String peopleNumber){
		this.peopleNumber = peopleNumber;
	}
	public String getPeopleNumber() {
		return this.peopleNumber;
	}
	public void setBasePrice(String basePrice){
		this.basePrice = basePrice;
	}
	public String getBasePrice() {
		return this.basePrice;
	}
}
