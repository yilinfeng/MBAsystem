package com.earl.mbasystem.domain.salaryorder;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.annotation.ReturnValue;
import com.earl.mbasystem.domain.base.BaseAction;
import com.earl.mbasystem.domain.department.DepartmentPo;
import com.earl.mbasystem.vo.DwzVo;

/**
 * 
 * 用途+action 如Demo+Action-->DemoAction
 * 
 * @author Administrator
 * 
 */
@Controller(value = "salaryOrderAction")
@Scope(value = "prototype")
public class SalaryOrderAction extends BaseAction<SalaryOrderPo> {

	/**
	 * 
	 */
	
	List<SalaryOrderPo> salaryOrderList;
	
	List<DepartmentPo> departmentList;
	
	private static final long serialVersionUID = 3293435262298029608L;

	protected Object result;

	
	public List<SalaryOrderPo> getSalaryOrderList() {
		return salaryOrderList;
	}

	public void setSalaryOrderList(List<SalaryOrderPo> salaryOrderList) {
		this.salaryOrderList = salaryOrderList;
	}

	public List<DepartmentPo> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(List<DepartmentPo> departmentList) {
		this.departmentList = departmentList;
	}

	@ReturnValue //返回实体对象，或者其他任意对象
	public Object getResultMessage() {
		return result;
	}

	// 下面填写业务逻辑

	/**
	 * 得到未处理的薪酬订单（系统未支付）
	 * @author 黄祥谦.
	 * @return
	 */
	public String allUnDealSalaryOrder(){
		//TODO 未操作
		salaryOrderList = salaryOrderServer.allUnDealSalaryOrder();
		return "salaryOrderList";
	}
	
	/**
	 * 未复查薪酬订单
	 * @author 黄祥谦.
	 * @return
	 */
	public String allUncheckSalaryOrder(){
		//TODO 未操作
		salaryOrderList = salaryOrderServer.allUncheckSalaryOrder();
		departmentList = departmentServer.findAll();
		return "salaryOrderList";
	}
	
	/**
	 * 已经处理的薪酬订单（历史订单）
	 * @author 黄祥谦.
	 * @return
	 */
	public String allcheckOrder(){
		salaryOrderList = salaryOrderServer.allcheckSalaryOrder();
		departmentList = departmentServer.findAll();
		return "historysalaryOrderList";
	}
	
	/**
	 * 生成薪酬订单.
	 * @author 黄祥谦.
	 */
	public void generateOrder(){
		salaryOrderServer.generatOrder();
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("生成成功");
		dwzVo.setCallbackType("closeCurrent");//转发
		result = dwzVo;
	}
}
