package com.earl.mbasystem.domain.salaryorder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;
import com.earl.mbasystem.domain.salaryorderdetail.SalaryOrderDetailDao;
import com.earl.mbasystem.domain.salaryorderdetail.SalaryOrderDetailPo;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
 @Service(value = "salaryOrderService")
public class SalaryOrderServiceImpl extends BaseServiceImpl<SalaryOrderPo> implements
		SalaryOrderService {

	@Resource(name = "salaryOrderDao")
	SalaryOrderDao salaryOrderDao;
	
	@Resource(name = "salaryOrderDetailDao")
	SalaryOrderDetailDao salaryOrderDetailDao;

	@PostConstruct
	public void initBaseDao(){
		baseDao = salaryOrderDao;
	}

	@Override
	public void generatOrder() {
		// TODO 未测试.
		List<SalaryOrderPo> generatOrder = salaryOrderDao.generatOrder();
		for (SalaryOrderPo salaryOrderPo : generatOrder) {
			salaryOrderPo.setHaveRecheck(false);
			salaryOrderPo.setHaveDeal(false);
			Long department1 = salaryOrderPo.getDepartment1();
			Long department2 = salaryOrderPo.getDepartment2();
			Long department3 = salaryOrderPo.getDepartment3();
			String tmp = department1+""+department2+""+department3;
			Date date = new Date();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			String str=sdf.format(date); 
			salaryOrderPo.setSalaryOrderNo(tmp+str);
			List<SalaryOrderDetailPo> salaryOrderDetailList = salaryOrderDetailDao.generatOrderDetail(department3);
			salaryOrderDao.saveOrder(salaryOrderPo, salaryOrderDetailList);
		}
		
	}

	@Override
	public List<SalaryOrderPo> allUnDealSalaryOrder() {
		// TODO 未测试.
		List<SalaryOrderPo> salaryOrderList = salaryOrderDao.allUnDealSalaryOrder();
		return salaryOrderList;
	}

	@Override
	public List<SalaryOrderPo> allUncheckSalaryOrder() {
		// TODO 未测试.
		List<SalaryOrderPo> salaryOrderList = salaryOrderDao.allUncheckSalaryOrder();
		return salaryOrderList;
	}

	@Override
	public List<SalaryOrderPo> allcheckSalaryOrder() {
		List<SalaryOrderPo> salaryOrderList = salaryOrderDao.allcheckSalaryOrder();
		return salaryOrderList;
	}
	
}
