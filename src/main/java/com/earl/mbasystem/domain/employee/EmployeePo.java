package com.earl.mbasystem.domain.employee;

import java.util.Date;

import com.earl.mbasystem.annotation.IdAnnotatioin;


public class EmployeePo{


	/**
	 * 字段描述：Long 
	 * 字段类型：employeeId  
	 */
	@IdAnnotatioin
	private Long employeeId ;
	
	/**
	 * 字段描述：Long 
	 * 字段类型：department1  
	 */
	private Long department1 ;
	
	/**
	 * 字段描述：Long 
	 * 字段类型：department2  
	 */
	private Long department2 ;
	
	/**
	 * 字段描述：Long 
	 * 字段类型：department3  
	 */
	private Long department3 ;
	
	/**
	 * 字段描述：Long 
	 * 字段类型：jopType  
	 */
	private Long jobType ;
	
	/**
	 * 字段描述：Long 
	 * 字段类型：jopName  
	 */
	private Long jobName ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：professionalRanks  
	 */
	private String professionalRanks ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：employeeName  
	 */
	private String employeeName ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：gender  
	 */
	private String gender ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：email  
	 */
	private String email ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：phone  
	 */
	private String phone ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：address  
	 */
	private String address ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：zipcode  
	 */
	private String zipcode ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：nationality  
	 */
	private String nationality ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：homeplace  
	 */
	private String homeplace ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：birthday  
	 */
	private String birthday ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：nation  
	 */
	private String nation ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：faith  
	 */
	private String faith ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：politicalStatus  
	 */
	private String politicalStatus ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：identityNumber  
	 */
	private String identityNumber ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：socialSecurityNumber  
	 */
	private String socialSecurityNumber ;
	
	/**
	 * 字段描述：Integer 
	 * 字段类型：age  
	 */
	private Integer age ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：educationalBackground  
	 */
	private String educationalBackground ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：majorIn  
	 */
	private String majorIn ;
	
	/**
	 * 字段描述：Long 
	 * 字段类型：salaryStandard  
	 */
	private Long salaryStandard ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：bankOfDeposit  
	 */
	private String bankOfDeposit ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：bankAccount  
	 */
	private String bankAccount ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：speciality  
	 */
	private String speciality ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：hobby  
	 */
	private String hobby ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：curriculumvitae  
	 */
	private String curriculumvitae ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：familyInfo  
	 */
	private String familyInfo ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：remarks  
	 */
	private String remarks ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：operater  
	 */
	private String operater ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：registerTime  
	 */
	private Date registerTime;
	
	/**
	 * 字段描述：String 
	 * 字段类型：photo  
	 */
	private String photo ;
	
	/**
	 * 字段描述：Boolean 
	 * 字段类型：havarecheck  
	 */
	private Boolean havarecheck ;
	
	/**
	 * 字段描述：Boolean 
	 * 字段类型：delete  
	 */
	private Boolean isDelete ;
	
	public void setEmployeeId(Long employeeId){
		this.employeeId = employeeId;
	}
	public Long getEmployeeId() {
		return this.employeeId;
	}
	public void setDepartment1(Long department1){
		this.department1 = department1;
	}
	public Long getDepartment1() {
		return this.department1;
	}
	public void setDepartment2(Long department2){
		this.department2 = department2;
	}
	public Long getDepartment2() {
		return this.department2;
	}
	public void setDepartment3(Long department3){
		this.department3 = department3;
	}
	public Long getDepartment3() {
		return this.department3;
	}
	public Long getJobType() {
		return jobType;
	}
	public void setJobType(Long jobType) {
		this.jobType = jobType;
	}
	public Long getJobName() {
		return jobName;
	}
	public void setJobName(Long jobName) {
		this.jobName = jobName;
	}
	public void setProfessionalRanks(String professionalRanks){
		this.professionalRanks = professionalRanks;
	}
	public String getProfessionalRanks() {
		return this.professionalRanks;
	}
	public void setEmployeeName(String employeeName){
		this.employeeName = employeeName;
	}
	public String getEmployeeName() {
		return this.employeeName;
	}
	public void setGender(String gender){
		this.gender = gender;
	}
	public String getGender() {
		return this.gender;
	}
	public void setEmail(String email){
		this.email = email;
	}
	public String getEmail() {
		return this.email;
	}
	public void setPhone(String phone){
		this.phone = phone;
	}
	public String getPhone() {
		return this.phone;
	}
	public void setAddress(String address){
		this.address = address;
	}
	public String getAddress() {
		return this.address;
	}
	public void setZipcode(String zipcode){
		this.zipcode = zipcode;
	}
	public String getZipcode() {
		return this.zipcode;
	}
	public void setNationality(String nationality){
		this.nationality = nationality;
	}
	public String getNationality() {
		return this.nationality;
	}
	public void setHomeplace(String homeplace){
		this.homeplace = homeplace;
	}
	public String getHomeplace() {
		return this.homeplace;
	}
	public void setBirthday(String birthday){
		this.birthday = birthday;
	}
	public String getBirthday() {
		return this.birthday;
	}
	public void setNation(String nation){
		this.nation = nation;
	}
	public String getNation() {
		return this.nation;
	}
	public void setFaith(String faith){
		this.faith = faith;
	}
	public String getFaith() {
		return this.faith;
	}
	public void setPoliticalStatus(String politicalStatus){
		this.politicalStatus = politicalStatus;
	}
	public String getPoliticalStatus() {
		return this.politicalStatus;
	}
	public void setIdentityNumber(String identityNumber){
		this.identityNumber = identityNumber;
	}
	public String getIdentityNumber() {
		return this.identityNumber;
	}
	public void setSocialSecurityNumber(String socialSecurityNumber){
		this.socialSecurityNumber = socialSecurityNumber;
	}
	public String getSocialSecurityNumber() {
		return this.socialSecurityNumber;
	}
	public void setAge(Integer age){
		this.age = age;
	}
	public Integer getAge() {
		return this.age;
	}
	public void setEducationalBackground(String educationalBackground){
		this.educationalBackground = educationalBackground;
	}
	public String getEducationalBackground() {
		return this.educationalBackground;
	}
	public void setMajorIn(String majorIn){
		this.majorIn = majorIn;
	}
	public String getMajorIn() {
		return this.majorIn;
	}
	public void setSalaryStandard(Long salaryStandard){
		this.salaryStandard = salaryStandard;
	}
	public Long getSalaryStandard() {
		return this.salaryStandard;
	}
	public void setBankOfDeposit(String bankOfDeposit){
		this.bankOfDeposit = bankOfDeposit;
	}
	public String getBankOfDeposit() {
		return this.bankOfDeposit;
	}
	public void setBankAccount(String bankAccount){
		this.bankAccount = bankAccount;
	}
	public String getBankAccount() {
		return this.bankAccount;
	}
	public void setSpeciality(String speciality){
		this.speciality = speciality;
	}
	public String getSpeciality() {
		return this.speciality;
	}
	public void setHobby(String hobby){
		this.hobby = hobby;
	}
	public String getHobby() {
		return this.hobby;
	}
	public void setCurriculumvitae(String curriculumvitae){
		this.curriculumvitae = curriculumvitae;
	}
	public String getCurriculumvitae() {
		return this.curriculumvitae;
	}
	public void setFamilyInfo(String familyInfo){
		this.familyInfo = familyInfo;
	}
	public String getFamilyInfo() {
		return this.familyInfo;
	}
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	public String getRemarks() {
		return this.remarks;
	}
	public void setOperater(String operater){
		this.operater = operater;
	}
	public String getOperater() {
		return this.operater;
	}
	public Date getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}
	public void setPhoto(String photo){
		this.photo = photo;
	}
	public String getPhoto() {
		return this.photo;
	}
	public void setHavarecheck(Boolean havarecheck){
		this.havarecheck = havarecheck;
	}
	public Boolean getHavarecheck() {
		return this.havarecheck;
	}
	public Boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	@Override
	public String toString() {
		return "EmployeePo [employeeId=" + employeeId + ", department1="
				+ department1 + ", department2=" + department2
				+ ", department3=" + department3 + ", jobType=" + jobType
				+ ", jobName=" + jobName + ", professionalRanks="
				+ professionalRanks + ", employeeName=" + employeeName
				+ ", gender=" + gender + ", email=" + email + ", phone="
				+ phone + ", address=" + address + ", zipcode=" + zipcode
				+ ", nationality=" + nationality + ", homeplace=" + homeplace
				+ ", birthday=" + birthday + ", nation=" + nation + ", faith="
				+ faith + ", politicalStatus=" + politicalStatus
				+ ", identityNumber=" + identityNumber
				+ ", socialSecurityNumber=" + socialSecurityNumber + ", age="
				+ age + ", educationalBackground=" + educationalBackground
				+ ", majorIn=" + majorIn + ", salaryStandard=" + salaryStandard
				+ ", bankOfDeposit=" + bankOfDeposit + ", bankAccount="
				+ bankAccount + ", speciality=" + speciality + ", hobby="
				+ hobby + ", curriculumvitae=" + curriculumvitae
				+ ", familyInfo=" + familyInfo + ", remarks=" + remarks
				+ ", operater=" + operater + ", registerTime=" + registerTime
				+ ", photo=" + photo + ", havarecheck=" + havarecheck
				+ ", isDelete=" + isDelete + "]";
	}
}
