package com.earl.mbasystem.domain.employee;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.earl.mbasystem.domain.base.BaseDaoImpl;


/**
 * 
 * 
 */
@Repository("employeeDao")
public class EmployeeDaoImpl extends BaseDaoImpl<EmployeePo> implements EmployeeDao {

	@Override
	public List<EmployeePo> unRecheckEmployee() {
		// TODO 未测试.
		String hql = "from EmployeePo where havarecheck=false";
		@SuppressWarnings("unchecked")
		List<EmployeePo> list = getCurrentSession().createQuery(hql).list();
		return list;
	}

	@Override
	public List<EmployeePo> pointStatusEmployee(Boolean deleted) {
		// TODO 未测试.
		String hql = "from EmployeePo where isDelete =:deleted";
		@SuppressWarnings("unchecked")
		List<EmployeePo> list = getCurrentSession().createQuery(hql).setBoolean("deleted", deleted).list();
		return list;
	}

	@Override
	public List<EmployeePo> nomalEmployee(Boolean undeleted) {
		// TODO 未测试.
		String hql = "from EmployeePo where isDelete =false and havarecheck=true";
		@SuppressWarnings("unchecked")
		List<EmployeePo> list = getCurrentSession().createQuery(hql).list();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EmployeePo> select(Long department3, Long jobName,
			DateQuery dateObject) {
		// TODO 未测试.
			Criteria ca = getCurrentSession().createCriteria(EmployeePo.class);
			if(department3!=null){
				ca.add( Restrictions.eq("department3", department3));
			}
			if(jobName!=null){
				ca.add( Restrictions.eq("jobName", jobName));
			}
			ca.add( Restrictions.between("registerTime", dateObject.begdate, dateObject.enddate)); 
			return ca.list();
	}
	
}