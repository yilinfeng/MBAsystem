package com.earl.mbasystem.domain.employee;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseDao;

public interface EmployeeDao extends BaseDao<EmployeePo>{

	/**
	 * 得到所有未进行复核的员工
	 * @author 黄祥谦.
	 * @return
	 */
	List<EmployeePo> unRecheckEmployee();

	/**
	 * 得到指定状态的员工集合.
	 * @author 黄祥谦.
	 * @param deleted
	 * @return
	 */
	List<EmployeePo> pointStatusEmployee(Boolean deleted);

	/**
	 * 复核通过，并且未被删除
	 * @author 黄祥谦.
	 * @param undeleted
	 * @return
	 */
	List<EmployeePo> nomalEmployee(Boolean undeleted);

	/**
	 * 查询.
	 * @author 黄祥谦.
	 * @param department3
	 * @param jobName
	 * @param begdate
	 * @return
	 */
	List<EmployeePo> select(Long department3, Long jobName, DateQuery begdate);

}
