package com.earl.mbasystem.domain.employee;

import java.util.Date;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.annotation.ReturnValue;
import com.earl.mbasystem.domain.base.BaseAction;
import com.earl.mbasystem.domain.department.DepartmentPo;
import com.earl.mbasystem.domain.position.PositionPo;
import com.earl.mbasystem.domain.salarystander.SalaryStanderPo;
import com.earl.mbasystem.domain.user.UserPo;
import com.earl.mbasystem.security.shiro.ShiroPrincipal;
import com.earl.mbasystem.vo.DwzVo;

/**
 * 
 * 用途+action 如Demo+Action-->DemoAction
 * 
 * @author Administrator
 * 
 */
@Controller(value = "employeeAction")
@Scope(value = "prototype")
public class EmployeeAction extends BaseAction<EmployeePo> {

	EmployeePo employee;
	
	UserPo currentUser;
	
	List<DepartmentPo> departmentList;
	
	List<EmployeePo> employeeList;
	
	DepartmentPo departmentB;
	
	DepartmentPo departmentC;
	
	Date begdate;
	
	Date enddate;
	
	/**
	 * 职位类别
	 */
	List<PositionPo> jobTypeList;
	
	List<PositionPo> jobNameList;
	/**
	 * 薪酬标准
	 */
	List<SalaryStanderPo> salaryStanderList;
	
	public Date getBegdate() {
		return begdate;
	}

	public UserPo getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(UserPo currentUser) {
		this.currentUser = currentUser;
	}

	public void setBegdate(Date begdate) {
		this.begdate = begdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

 	public List<PositionPo> getJobNameList() {
		return jobNameList;
	}

	public void setJobNameList(List<PositionPo> jobNameList) {
		this.jobNameList = jobNameList;
	}

	public List<DepartmentPo> getDepartmentList() {
		return departmentList;
	}

	public DepartmentPo getDepartmentB() {
		return departmentB;
	}

	public void setDepartmentB(DepartmentPo departmentB) {
		this.departmentB = departmentB;
	}

	public DepartmentPo getDepartmentC() {
		return departmentC;
	}

	public void setDepartmentC(DepartmentPo departmentC) {
		this.departmentC = departmentC;
	}

	public void setDepartmentList(List<DepartmentPo> departmentList) {
		this.departmentList = departmentList;
	}

	public List<PositionPo> getJobTypeList() {
		return jobTypeList;
	}

	public void setJobTypeList(List<PositionPo> jobTypeList) {
		this.jobTypeList = jobTypeList;
	}

	public List<SalaryStanderPo> getSalaryStanderList() {
		return salaryStanderList;
	}

	public void setSalaryStanderList(List<SalaryStanderPo> salaryStanderList) {
		this.salaryStanderList = salaryStanderList;
	}

	public EmployeePo getEmployee() {
		return employee;
	}

	public List<EmployeePo> getEmployeeList() {
		return employeeList;
	}
	public void setEmployee(EmployeePo employee) {
		this.employee = employee;
	}

	public void setEmployeeList(List<EmployeePo> employeeList) {
		this.employeeList = employeeList;
	}

	private static final long serialVersionUID = 3293435262298029608L;

	protected Object result;

	@ReturnValue //返回实体对象，或者其他任意对象
	public Object getResultMessage() {
		return result;
	}

	// 下面填写业务逻辑

	/**
	 * 添加员工对象.
	 * @author 黄祥谦.
	 */
	public void addEmployee() {
		employeeServer.addEmployee(model);
		ShiroPrincipal principal = (ShiroPrincipal) SecurityUtils.getSubject().getPrincipal();
		currentUser = principal.getUser();
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("添加成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("employee_nomalEmployee.action");
		result = dwzVo;
	}
	
	/**
	 * 更新员工非空属性.
	 * @author 黄祥谦.
	 */
	public void udpateEmployee(){
		employeeServer.udpateEmployee(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("更新成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("employee_nomalEmployee.action");
		result = dwzVo;
	}
	
	/**
	 * 预处理添加表单，得到特定的预备对象.
	 * @author 黄祥谦.
	 * @return
	 */
	public String preAddEmployee(){
		salaryStanderList = salaryStanderServer.allSalaryStander();
		jobTypeList = positionServer.allTopPosition();
		departmentList = departmentServer.allTopDepartment();
		return "preAddEmployee";
	}
	
	/**
	 * 复查通过.
	 * 这里的复查，需要传入参数，operater //登记人
	 * @author 黄祥谦.
	 */
	public void recheckPass(){
		model.setHavarecheck(true);
		employeeServer.recheckPass(model.getEmployeeId());
		ShiroPrincipal principal = (ShiroPrincipal) SecurityUtils.getSubject().getPrincipal();
		currentUser = principal.getUser();
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("复核成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("employee_unRecheckEmployee.action");
		result = dwzVo;
	}
	
	/**
	 * 得到未复核员工集合.
	 * @author 黄祥谦.
	 */
	public String unRecheckEmployee(){
		employeeList = employeeServer.unRecheckEmployee();
		return "uncheckEmployeeList";
	}

	/**
	 * 删除员工对象
	 * @author 黄祥谦.
	 */
	public void deleteEmployee(){
		employeeServer.deleteEmployee(model.getEmployeeId());
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("删除成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("employee_nomalEmployee.action");
		result = dwzVo;
	}
	
	/**
	 * 更新员工的预处理操作.
	 * @author 黄祥谦.
	 * @return
	 */
	public String aEmployee(){
		salaryStanderList = salaryStanderServer.allSalaryStander();
		jobTypeList = positionServer.allTopPosition();
		jobNameList = positionServer.allNextPosition();
		departmentList = departmentServer.allTopDepartment();
		employee = employeeServer.get(model.getEmployeeId());
		departmentB = departmentServer.get(employee.getDepartment2());
		departmentC = departmentServer.get(employee.getDepartment3());
		ShiroPrincipal principal = (ShiroPrincipal) SecurityUtils.getSubject().getPrincipal();
		currentUser = principal.getUser();
		return "employee";
	}
	
	/**
	 * 恢复已经删除的员工
	 * @author 黄祥谦.
	 */
	public void rebackEmployee(){
		employeeServer.rebackEmployee(model.getEmployeeId());
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("恢复成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("employee_nomalEmployee.action");
		result = dwzVo;
		
	}
	
	/**
	 * 得到所有员工.
	 * @author 黄祥谦.
	 * @return
	 */
	public String allEmployee(){
		employeeList = employeeServer.findAll();
		System.out.println(employeeList);
		return "employeeList";
	}
	
	/**
	 * 查找已经通过审核的员工
	 * @author 黄祥谦.
	 * @return
	 */
	public String nomalEmployee(){
		employeeList = employeeServer.nomalEmployee();
		return "employeeList";
	}
	
	/**
	 * 已经被软删除的员工对象
	 * @author 黄祥谦.
	 * @return
	 */
	public String beDeleteEmployee(){
		if(employeeList != null){
			employeeList.clear();
		}
		employeeList = employeeServer.beDeleteEmployee();
		return "beDeleteEmployeeList";
	}
	
	/**
	 * 预处理搜索
	 * @author 黄祥谦.
	 * @return
	 */
	public String preSelect(){
		departmentList = departmentServer.allTopDepartment();
		jobTypeList = positionServer.allTopPosition();
		return "preSelect";
	}
	
	/**
	 * 搜索.
	 * @author 黄祥谦.
	 * @return
	 */
	public String select(){
		employeeList = employeeServer.select(model.getDepartment3(),model.getJobName(),begdate,enddate);
		return "employeeList";
	}
}
