package com.earl.mbasystem.domain.employee;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.shiro.codec.Hex;
import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;
import com.earl.mbasystem.domain.user.UserPo;
import com.earl.mbasystem.util.Digests;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
 @Service(value = "employeeService")
public class EmployeeServiceImpl extends BaseServiceImpl<EmployeePo> implements
		EmployeeService {

	 /**
	  * 得到符合业务的主键
	  */
	private static Long nextId=0L;
	
	private static final Boolean deleted=true;
	
	private static final Boolean undeleted=false;
	 
	@Resource(name = "employeeDao")
	EmployeeDao employeeDao;

	@PostConstruct
	public void initBaseDao(){
		baseDao = employeeDao;
	}

	/**
	 * 返回下一个id
	 * @author 黄祥谦.
	 * @param tmp
	 * @return
	 */
	private String nextId(String tmp){
		nextId+=1;
	    String result = tmp + String.format("%06d", nextId);
	    System.out.println(result);
		return result;
	}

	@Override
	public void addEmployee(EmployeePo model) {
			Long department1 = model.getDepartment1();
			Long department2 = model.getDepartment2();
			Long department3 = model.getDepartment3();
			String tmp = department1+""+department2+""+department3;
			String nextId2 = nextId(tmp);
			model.setEmployeeId(Long.valueOf(nextId2));
			model.setIsDelete(false);
			model.setHavarecheck(false);//默认未进行复查
			employeeDao.save(model);
	}
	
	@Override
	public List<EmployeePo> unRecheckEmployee() {
		List<EmployeePo> employeeList = employeeDao.unRecheckEmployee();
		return employeeList;
	}

	@Override
	public void deleteEmployee(Long employeeId) {
		EmployeePo employeePo = employeeDao.get(employeeId);
		Boolean havarecheck = employeePo.getHavarecheck();
		if(!havarecheck){
		}else{
			employeePo.setIsDelete(true);
		}
		employeeDao.update(employeePo);
	}

	@Override
	public void rebackEmployee(Long employeeId) {
		EmployeePo employeePo = employeeDao.get(employeeId);
		employeePo.setIsDelete(false);
		employeePo.setHavarecheck(false);
		employeeDao.update(employeePo);

	}

	@Override
	public List<EmployeePo> nomalEmployee() {
		List<EmployeePo> employeeList = employeeDao.nomalEmployee(undeleted);
		return employeeList;
	}

	@Override
	public List<EmployeePo> beDeleteEmployee() {
		List<EmployeePo> employeeList = employeeDao.pointStatusEmployee(deleted);
		return employeeList;
	}

	@Override
	public void recheckPass(Long employeeId) {
		EmployeePo employee = employeeDao.get(employeeId);
		employee.setHavarecheck(true);
		UserPo userPo2 = userDao.get(employee.getEmployeeId());
		if(userPo2 ==null){
			UserPo userPo = new UserPo();
			userPo.setUserId(employeeId);
			userPo.setEmployeeName(employee.getEmployeeName());
			
			byte[] sha1 = Digests.sha1(employee.getBirthday().getBytes(),null,1024);//散列次数 1024
			
			String encodeToString = Hex.encodeToString(sha1);
			userPo.setPassword(encodeToString);//加密用户密码
			userDao.save(userPo);
		}
		employeeDao.update(employee);
	}

	@Override
	public void udpateEmployee(EmployeePo model) {
		model.setHavarecheck(false);
		employeeDao.updateWithNotNullProperties(model);
	}

	@Override
	public List<EmployeePo> select(Long department3, Long jobName,
			Date begdate, Date enddate) {
		// TODO 未测试.
		DateQuery dateQuery = DateQuery.getDateQuery(begdate, enddate);
		if(dateQuery == null){
			EmployeePo employeePo = new EmployeePo();
			employeePo.setDepartment3(department3);
			employeePo.setJobName(jobName);
			return employeeDao.findByGivenCriteria(employeePo);
		}else{
			
			return employeeDao.select(department3,jobName,dateQuery);
		}
	}
}
