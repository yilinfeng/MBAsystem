package com.earl.mbasystem.domain.employee;

import com.earl.mbasystem.domain.base.BaseService;

import java.util.Date;
import java.util.List;

public interface EmployeeService extends BaseService<EmployeePo>{

	/**
	 * 添加员工信息.
	 * @author 黄祥谦.
	 * @param model
	 */
	void addEmployee(EmployeePo model);

	List<EmployeePo> unRecheckEmployee();

	/**
	 * 软删除员工信息.
	 * @author 黄祥谦.
	 * @param employeeId
	 */
	void deleteEmployee(Long employeeId);

	/**
	 * 得到所有未被删除的员工
	 * @author 黄祥谦.
	 * @return
	 */
	List<EmployeePo> nomalEmployee();

	/**
	 * 得到已经被删除的员工
	 * @author 黄祥谦.
	 * @return
	 */
	List<EmployeePo> beDeleteEmployee();

	/**
	 * 复查员工信息.
	 * @author 黄祥谦.
	 * @param employeeId
	 */
	void recheckPass(Long employeeId);

	/**
	 * 恢复已经删除的员工.
	 * @author 黄祥谦.
	 * @param employeeId
	 */
	void rebackEmployee(Long employeeId);

	/**
	 * 更新员工，更新成功，修改状态未复核
	 * @author 黄祥谦.
	 * @param model
	 */
	void udpateEmployee(EmployeePo model);

	/**
	 * 查询
	 * @author 黄祥谦.
	 * @param department3
	 * @param jobName
	 * @param begdate
	 * @param enddate
	 * @return
	 */
	List<EmployeePo> select(Long department3, Long jobName, Date begdate,
			Date enddate);

}
