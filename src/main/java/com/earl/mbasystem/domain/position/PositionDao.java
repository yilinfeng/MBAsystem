package com.earl.mbasystem.domain.position;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseDao;

public interface PositionDao extends BaseDao<PositionPo>{

	/**
	 * 得到所有顶级职称.
	 * @author 黄祥谦.
	 * @return
	 */
	List<PositionPo> allTopPosition();

	List<PositionPo> nextLevelPosition(Long positionId);

	/**
	 * 所有二级类别.
	 * @author 黄祥谦.
	 * @return
	 */
	List<PositionPo> allNextPosition();

}
