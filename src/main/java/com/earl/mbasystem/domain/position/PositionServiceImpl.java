package com.earl.mbasystem.domain.position;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
 @Service(value = "positionService")
public class PositionServiceImpl extends BaseServiceImpl<PositionPo> implements
		PositionService {

	@Resource(name = "positionDao")
	PositionDao positionDao;

	@PostConstruct
	public void initBaseDao(){
		baseDao = positionDao;
	}

	@Override
	public List<PositionPo> allTopPosition() {
		// TODO 未测试.
		List<PositionPo> positionList = positionDao.allTopPosition();
		return positionList;
	}

	@Override
	public List<PositionPo> nextLevelPosition(Long positionId) {
		// TODO 未测试.
 		List<PositionPo> positionList = positionDao.nextLevelPosition(positionId);
		return positionList;
	}

	@Override
	public List<PositionPo> allNextPosition() {
		// TODO 未测试.
		List<PositionPo> positionList = positionDao.allNextPosition();
		return positionList;
	}

}
