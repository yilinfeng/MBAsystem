package com.earl.mbasystem.domain.position;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.annotation.ReturnValue;
import com.earl.mbasystem.domain.base.BaseAction;
import com.earl.mbasystem.vo.DwzVo;

/**
 * 
 * 用途+action 如Demo+Action-->DemoAction
 * 
 * @author Administrator
 * 
 */
@Controller(value = "positionAction")
@Scope(value = "prototype")
public class PositionAction extends BaseAction<PositionPo> {

	
	PositionPo position;
	
	List<PositionPo> positionList;
	
	public PositionPo getPosition() {
		return position;
	}

	public void setPosition(PositionPo position) {
		this.position = position;
	}

	public List<PositionPo> getPositionList() {
		return positionList;
	}

	public void setPositionList(List<PositionPo> positionList) {
		this.positionList = positionList;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3293435262298029608L;

	protected Object result;

	@ReturnValue //返回实体对象，或者其他任意对象
	public Object getResultMessage() {
		return result;
	}

	// 下面填写业务逻辑

	/**
	 * 添加对象.
	 * @author 黄祥谦.
	 */
	public void addPosition() {
		positionServer.save(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("添加成功");
		dwzVo.setCallbackType("closeCurrent");//转发
		result = dwzVo;
	}
	
	public String preAddJobName(){
		positionList = positionServer.allTopPosition();
		return "preAddJobName";
	}
	
	public void updatePosition(){
		positionServer.updateWithNotNullProperties(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("更新成功");
		dwzVo.setCallbackType("closeCurrent");//转发
		result = dwzVo;
	}
	
	public void deletePosition(){
		positionServer.deleteById(model.getPositionId());
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("删除成功");
		dwzVo.setCallbackType("closeCurrent");//转发
		result = dwzVo;
	}
	
	/**
	 * 职位类型.
	 * @author 黄祥谦.
	 * @return
	 */
	public String aTypePosition(){
		position = positionServer.get(model.getPositionId());
		return "atypePosition";
	}
	
	/**
	 * 职位名称
	 * @author 黄祥谦.
	 * @return
	 */
	public String aNamePosition(){
		position = positionServer.get(model.getPositionId());
		positionList = positionServer.allTopPosition();
		return "anamePosition";
	}
	
	/**
	 * 得到所有顶级 职称
	 * @author 黄祥谦.
	 */
	public String allTopPosition(){
		positionList = positionServer.allTopPosition();
		return "jobtypeList";
	}
	
	public String allNextPosition(){
		positionList = positionServer.allNextPosition();
		return "jobnameList";
	}
	
	/**
	 * 得到指定父级职称下的二级职称
	 * @author 黄祥谦.
	 */
	public void nextLevelPosition(){
		positionList = positionServer.nextLevelPosition(model.getPositionId());
		
		List<List<String>> positionVoList= new ArrayList<List<String>>();
		for (PositionPo positionPo : positionList) {
			List<String> list = new ArrayList<String>();
			list.add(positionPo.getPositionId().toString());
			list.add(positionPo.getPositionName());
			positionVoList.add(list);
		}
		result=positionVoList;
	}
}
