package com.earl.mbasystem.domain.position;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.earl.mbasystem.domain.base.BaseDaoImpl;


/**
 * 
 * 
 */
@Repository("positionDao")
public class PositionDaoImpl extends BaseDaoImpl<PositionPo> implements PositionDao {

	@Override
	public List<PositionPo> allTopPosition() {
		// TODO 未测试.
		String hql = "from PositionPo where parentId=null";
		@SuppressWarnings("unchecked")
		List<PositionPo> list = getCurrentSession().createQuery(hql).list();
		return list;
	}
	// property constants

	@Override
	public List<PositionPo> nextLevelPosition(Long positionId) {
		// TODO 未测试.
		String hql = "from PositionPo where parentId=:positionId";
		@SuppressWarnings("unchecked")
		List<PositionPo> list = getCurrentSession().createQuery(hql).setLong("positionId", positionId).list();
		return list;
	}

	@Override
	public List<PositionPo> allNextPosition() {
		// TODO 未测试.
		String hql = "from PositionPo where parentId != null";
		@SuppressWarnings("unchecked")
		List<PositionPo> list = getCurrentSession().createQuery(hql).list();
		return list;
	}
	
}