package com.earl.mbasystem.domain.position;

import com.earl.mbasystem.annotation.IdAnnotatioin;


public class PositionPo{


	/**
	 * 字段描述：Long 
	 * 字段类型：positionId  
	 */
	@IdAnnotatioin
	private Long positionId ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：positionName  
	 */
	private String positionName;
	
	/**
	 * 字段描述：String 
	 * 字段类型：describe  
	 */
	private String describeword ;
	
	/**
	 * 字段描述：Long 
	 * 字段类型：parentId  
	 */
	private Long parentId ;
	
	
	public void setPositionId(Long positionId){
		this.positionId = positionId;
	}
	public Long getPositionId() {
		return this.positionId;
	}
	public void setPositionName(String positionName){
		this.positionName = positionName;
	}
	public String getPositionName() {
		return this.positionName;
	}
	public String getDescribeword() {
		return describeword;
	}
	public void setDescribeword(String describeword) {
		this.describeword = describeword;
	}
	public void setParentId(Long parentId){
		this.parentId = parentId;
	}
	public Long getParentId() {
		return this.parentId;
	}
	@Override
	public String toString() {
		return "PositionPo [positionId=" + positionId + ", positionName="
				+ positionName + ", describeword=" + describeword
				+ ", parentId=" + parentId + "]";
	}
}
