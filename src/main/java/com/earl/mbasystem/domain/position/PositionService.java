package com.earl.mbasystem.domain.position;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseService;

public interface PositionService extends BaseService<PositionPo>{

	/**
	 * 得到所有顶级职称.
	 * @author 黄祥谦.
	 * @return
	 */
	List<PositionPo> allTopPosition();

	List<PositionPo> nextLevelPosition(Long positionId);

	/**
	 * 得到所有非顶级类型.
	 * @author 黄祥谦.
	 * @return
	 */
	List<PositionPo> allNextPosition();

}
