package com.earl.mbasystem.domain.privilege;

import org.springframework.stereotype.Repository;

import com.earl.mbasystem.domain.base.BaseDaoImpl;


/**
 * 
 * 
 */
@Repository("privilegeDao")
public class PrivilegeDaoImpl extends BaseDaoImpl<PrivilegePo> implements PrivilegeDao {
	// property constants
	
}