package com.earl.mbasystem.domain.privilege;


public class PrivilegePo{


	/**
	 * 字段描述：Long 
	 * 字段类型：privilegeId  
	 */
	private Long privilegeId ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：privilegeName  
	 */
	private String privilegeName ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：url  
	 */
	private String url ;
	
	
	public void setPrivilegeId(Long privilegeId){
		this.privilegeId = privilegeId;
	}
	public Long getPrivilegeId() {
		return this.privilegeId;
	}
	public void setPrivilegeName(String privilegeName){
		this.privilegeName = privilegeName;
	}
	public String getPrivilegeName() {
		return this.privilegeName;
	}
	public void setUrl(String url){
		this.url = url;
	}
	public String getUrl() {
		return this.url;
	}
}
