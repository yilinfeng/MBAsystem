package com.earl.mbasystem.domain.privilege;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.annotation.ReturnValue;
import com.earl.mbasystem.domain.base.BaseAction;
import com.earl.mbasystem.vo.DwzVo;

/**
 * 
 * 用途+action 如Demo+Action-->DemoAction
 * 
 * @author Administrator
 * 
 */
@Controller(value = "privilegeAction")
@Scope(value = "prototype")
public class PrivilegeAction extends BaseAction<PrivilegePo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3293435262298029608L;

	List<PrivilegePo> privilegeList;
	
	protected Object result;

	private PrivilegePo privilege;

	public PrivilegePo getPrivilege() {
		return privilege;
	}

	public void setPrivilege(PrivilegePo privilege) {
		this.privilege = privilege;
	}

	@ReturnValue //返回实体对象，或者其他任意对象
	public Object getResultMessage() {
		return result;
	}

	// 下面填写业务逻辑

	public List<PrivilegePo> getPrivilegeList() {
		return privilegeList;
	}

	public void setPrivilegeList(List<PrivilegePo> privilegeList) {
		this.privilegeList = privilegeList;
	}

	/**
	 * 添加对象.
	 * @author 黄祥谦.
	 */
	public void addPrivilege() {
		privilegeServer.save(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setMessage("添加成功");
		dwzVo.setStatusCode("200"); 
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("privilege_findAll.action");
		result = dwzVo;
	}
	
	public void updatePrivilege(){
		privilegeServer.update(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setMessage("更新成功");
		dwzVo.setStatusCode("200"); 
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("privilege_findAll.action");
		result=dwzVo;
	}
	
	public void deletePrivilege(){
		privilegeServer.deleteById(model.getPrivilegeId());
		DwzVo dwzVo = new DwzVo();
		dwzVo.setMessage("删除成功");
		dwzVo.setStatusCode("200"); 
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("privilege_findAll.action");
		result=dwzVo;
	}
	
	public String aPrivilege(){
		privilege = privilegeServer.get(model.getPrivilegeId());
		return "aPrivilege";
	}
	
	public String findAll(){
		privilegeList = privilegeServer.findAll();
		return "privilegeList";
	}
}
