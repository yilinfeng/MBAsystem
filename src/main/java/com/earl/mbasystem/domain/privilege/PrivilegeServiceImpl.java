package com.earl.mbasystem.domain.privilege;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
 @Service(value = "privilegeService")
public class PrivilegeServiceImpl extends BaseServiceImpl<PrivilegePo> implements
		PrivilegeService {

	@Resource(name = "privilegeDao")
	PrivilegeDao privilegeDao;

	@PostConstruct
	public void initBaseDao(){
		baseDao = privilegeDao;
	}
	
}
