package com.earl.mbasystem.domain.role;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.annotation.ReturnValue;
import com.earl.mbasystem.domain.base.BaseAction;
import com.earl.mbasystem.domain.privilege.PrivilegePo;
import com.earl.mbasystem.vo.DwzVo;

/**
 * 
 * 用途+action 如Demo+Action-->DemoAction
 * 
 * @author Administrator
 * 
 */
@Controller(value = "roleAction")
@Scope(value = "prototype")
public class RoleAction extends BaseAction<RolePo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3293435262298029608L;

	protected Object result;

	RolePo role;
	
	List<RolePo> roleList;
	
	List<PrivilegePo> privilegeList;
	
	List<Long> privilegeId;
	
	public RolePo getRole() {
		return role;
	}

	public void setRole(RolePo role) {
		this.role = role;
	}

	public List<Long> getPrivilegeId() {
		return privilegeId;
	}

	public void setPrivilegeId(List<Long> privilegeId) {
		this.privilegeId = privilegeId;
	}

	public List<PrivilegePo> getPrivilegeList() {
		return privilegeList;
	}

	public void setPrivilegeList(List<PrivilegePo> privilegeList) {
		this.privilegeList = privilegeList;
	}

	@ReturnValue //返回实体对象，或者其他任意对象
	public Object getResultMessage() {
		return result;
	}

	// 下面填写业务逻辑

	public List<RolePo> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<RolePo> roleList) {
		this.roleList = roleList;
	}

	/**
	 * 添加对象.
	 * @author 黄祥谦.
	 */
	public void addRole() {
		roleServer.addRole(model,privilegeId);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setMessage("添加成功");
		dwzVo.setStatusCode("200"); 
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("employee_nomalEmployee.action");
		result=dwzVo;
	}
	
	public void updateRole(){
		roleServer.updateRole(model,privilegeId);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setMessage("更新成功");
		dwzVo.setStatusCode("200"); 
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("employee_nomalEmployee.action");
		result=dwzVo;
	}
	
	public String preAddRole(){
		privilegeList =privilegeServer.findAll();
		return "addrole";
	}
	
	public String preUpdate(){
		role = roleServer.get(model.getRoleId());
		role.setPrivilegeList(roleServer.getPrivilegeList(model.getRoleId()));
		privilegeList =privilegeServer.findAll();
		return "editRole";
		
	}
	
	public void deleteRole(){
		roleServer.deleteById(model.getRoleId());
		DwzVo dwzVo = new DwzVo();
		dwzVo.setMessage("删除成功");
		dwzVo.setStatusCode("200"); 
		result=dwzVo;
	}
	
	public String findAllRole(){
		roleList = roleServer.findAll();
		return "roleList";
	}
	
	
}
