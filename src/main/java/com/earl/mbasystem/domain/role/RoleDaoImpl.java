package com.earl.mbasystem.domain.role;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.earl.mbasystem.domain.base.BaseDaoImpl;
import com.earl.mbasystem.domain.privilege.PrivilegePo;
import com.earl.mbasystem.domain.roleprivilegemapping.RolePrivilegeManagePo;


/**
 * 
 * 
 */
@Repository("roleDao")
public class RoleDaoImpl extends BaseDaoImpl<RolePo> implements RoleDao {

	@Override
	public List<String> getByUser(Long userId) {
		// TODO 未测试.
		String sql = "select r.roleName from userrolemanage ur left outer join role r on ur.roleId=r.roleId where ur.userId=:userId";
		List<String> list = getCurrentSession().createSQLQuery(sql).setLong("userId", userId).list();
		
		return list;
	}
	// property constants

	@Override
	public List<RolePo> userRoleList(Long userId) {
		// TODO 未测试.
		String sql = "select ur.userRoleManageId,r.* from userrolemanage ur left outer join role r on ur.roleId=r.roleId where ur.userId=:userId";
		@SuppressWarnings("unchecked")
		List<Object[]> list = getCurrentSession().createSQLQuery(sql).setLong("userId", userId).list();
		ArrayList<RolePo> arrayList = new ArrayList<RolePo>();
		for (Object[] objects : list) {
			RolePo rolePo = new RolePo();
			rolePo.setUserroleId(((BigInteger)objects[0]).longValue());
			rolePo.setRoleId(((BigInteger)objects[1]).longValue());
			rolePo.setRoleName((String)objects[2]);
			arrayList.add(rolePo);
		}
		return arrayList;
	}

	@Override
	public void addRole(RolePo model, List<Long> privilegeId) {
		Long save = (Long)getCurrentSession().save(model);
		for (Long long1 : privilegeId) {
			RolePrivilegeManagePo rp = new RolePrivilegeManagePo();
			rp.setRoleId(save);
			rp.setPrivilegeId(long1);
			getCurrentSession().save(rp);
		}
	}

	@Override
	public List<PrivilegePo> getPrivilegeList(Long roleId) {
		// TODO 未测试.
		String sql = "select p.* from RolePrivilegeManage rp left outer join privilege p on rp.privilegeId=p.privilegeId where rp.roleId=:roleId";
		@SuppressWarnings("unchecked")
		List<Object[]> list = getCurrentSession().createSQLQuery(sql).setLong("roleId", roleId).list();
		ArrayList<PrivilegePo> arrayList = new ArrayList<PrivilegePo>();
		for (Object[] objects : list) {
			PrivilegePo privilege = new PrivilegePo();
			privilege.setPrivilegeId(((BigInteger)objects[0]).longValue());
			privilege.setPrivilegeName((String)objects[1]);
			privilege.setUrl((String)objects[2]);
			arrayList.add(privilege);
		}
		return arrayList;
	}

	@Override
	public void updateRole(RolePo model, List<Long> privilegeId) {
		// TODO 未测试.
		String sql = "delete from RolePrivilegeManagePo where roleId = :roleId";
		getCurrentSession().createQuery(sql).setLong("roleId", model.getRoleId()).executeUpdate();
		getCurrentSession().update(model);
		for (Long long1 : privilegeId) {
			RolePrivilegeManagePo rp = new RolePrivilegeManagePo();
			rp.setRoleId(model.getRoleId());
			rp.setPrivilegeId(long1);
			getCurrentSession().save(rp);
		}
	}

}