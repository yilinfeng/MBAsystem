package com.earl.mbasystem.domain.role;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseDao;
import com.earl.mbasystem.domain.privilege.PrivilegePo;

public interface RoleDao extends BaseDao<RolePo>{

	/**
	 * 查询所有用户角色
	 * @author 黄祥谦.
	 * @param userId
	 * @return
	 */
	List<String> getByUser(Long userId);

	List<RolePo> userRoleList(Long userId);

	/**
	 * 添加角色，同时添加权限.
	 * @author 黄祥谦.
	 * @param model
	 * @param privilegeId
	 */
	void addRole(RolePo model, List<Long> privilegeId);

	List<PrivilegePo> getPrivilegeList(Long roleId);

	/**
	 * 更新用户角色.
	 * @author 黄祥谦.
	 * @param model
	 * @param privilegeId
	 */
	void updateRole(RolePo model, List<Long> privilegeId);

}
