package com.earl.mbasystem.domain.role;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;
import com.earl.mbasystem.domain.privilege.PrivilegePo;
import com.earl.mbasystem.domain.user.UserDao;
import com.earl.mbasystem.security.shiro.ShiroPrincipal;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
 @Service(value = "roleService")
public class RoleServiceImpl extends BaseServiceImpl<RolePo> implements
		RoleService {

	@Resource(name = "roleDao")
	RoleDao roleDao;
	
	@Resource(name = "userDao")
	UserDao userDao;

	@PostConstruct
	public void initBaseDao(){
		baseDao = roleDao;
	}

	@Override
	public List<RolePo> userRoleList(Long userId) {
		// TODO 未测试.
		List<RolePo> roleList = roleDao.userRoleList(userId);
		return roleList;
	}

	@Override
	public void addRole(RolePo model, List<Long> privilegeId) {
		// TODO 未测试.
		roleDao.addRole(model,privilegeId);
	}

	@Override
	public List<PrivilegePo> getPrivilegeList(Long roleId) {
		// TODO 未测试.
		List<PrivilegePo> privilegeList = roleDao.getPrivilegeList(roleId);
		return privilegeList;
	}

	@Override
	public void updateRole(RolePo model, List<Long> privilegeId) {
		// TODO 未测试.
		roleDao.updateRole(model,privilegeId);
		Subject subject = SecurityUtils.getSubject();  
		ShiroPrincipal user= (ShiroPrincipal) subject.getPrincipal();
		
		List<String> authorities = userDao.getPrivilegeUrl(user.getUser().getUserId());
		List<String> rolelist = userDao.getRolesName(user.getUser().getUserId());
		user.setAuthorities(authorities);
		user.setRoles(rolelist);
	}
	
}
