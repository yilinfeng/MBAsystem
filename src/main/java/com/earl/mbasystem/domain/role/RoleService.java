package com.earl.mbasystem.domain.role;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseService;
import com.earl.mbasystem.domain.privilege.PrivilegePo;

public interface RoleService extends BaseService<RolePo>{

	/**
	 * 得到用户的角色
	 * @author 黄祥谦.
	 * @param userId
	 * @return
	 */
	List<RolePo> userRoleList(Long userId);

	/**
	 * 添加用户角色.
	 * @author 黄祥谦.
	 * @param model
	 * @param privilegeId 
	 */
	void addRole(RolePo model, List<Long> privilegeId);

	/**
	 * 
	 * @author 黄祥谦.
	 * @param roleId
	 * @return
	 */
	List<PrivilegePo> getPrivilegeList(Long roleId);

	/**
	 * 更新用户角色.
	 * @author 黄祥谦.
	 * @param model
	 * @param privilegeId
	 */
	void updateRole(RolePo model, List<Long> privilegeId);

	

	
	
	
}
