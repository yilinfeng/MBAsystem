package com.earl.mbasystem.domain.role;

import java.util.List;

import com.earl.mbasystem.domain.privilege.PrivilegePo;


public class RolePo{


	/**
	 * 字段描述：Long 
	 * 字段类型：RoleId  
	 */
	private Long roleId ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：RoleName  
	 */
	private String roleName ;
	
	private Long userroleId;
	
	private List<PrivilegePo> privilegeList;
	
	public List<PrivilegePo> getPrivilegeList() {
		return privilegeList;
	}
	public void setPrivilegeList(List<PrivilegePo> privilegeList) {
		this.privilegeList = privilegeList;
	}
	public Long getUserroleId() {
		return userroleId;
	}
	public void setUserroleId(Long userroleId) {
		this.userroleId = userroleId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
