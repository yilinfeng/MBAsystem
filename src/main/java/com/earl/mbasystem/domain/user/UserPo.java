package com.earl.mbasystem.domain.user;

import java.util.List;

import com.earl.mbasystem.domain.role.RolePo;


public class UserPo{


	/**
	 * 字段描述：Long 
	 * 字段类型：employeeId  
	 */
	private Long userId ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：employeeName  
	 */
	private String employeeName ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：password  
	 */
	private String password;
	
	private String roleList;
	
	private List<RolePo> roleArrayList;
	
	public Long getUserId() {
		return userId;
	}
	
	public List<RolePo> getRoleArrayList() {
		return roleArrayList;
	}

	public void setRoleArrayList(List<RolePo> roleArrayList) {
		this.roleArrayList = roleArrayList;
	}

	public String getRoleList() {
		return roleList;
	}

	public void setRoleList(String roleList) {
		this.roleList = roleList;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public void setEmployeeName(String employeeName){
		this.employeeName = employeeName;
	}
	public String getEmployeeName() {
		return this.employeeName;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public String getPassword() {
		return this.password;
	}
	@Override
	public String toString() {
		return "UserPo [userId=" + userId + ", employeeName=" + employeeName
				+ ", password=" + password + ", roleList=" + roleList + "]";
	}
}
