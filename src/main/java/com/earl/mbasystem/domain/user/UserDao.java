package com.earl.mbasystem.domain.user;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseDao;

public interface UserDao extends BaseDao<UserPo>{

	/**
	 * 通过名字得到用户信息.
	 * @author 黄祥谦.
	 * @param username
	 * @return
	 */
	UserPo getByName(String username);

	/**
	 * 得到用户拥有的权限名字
	 * @author 黄祥谦.
	 * @param userId
	 * @return
	 */
	List<String> getPrivilegeName(Long userId);

	/**
	 * 通过用户id得到用户角色.
	 * @author 黄祥谦.
	 * @param userId
	 * @return
	 */
	List<String> getRolesName(Long userId);

	/**
	 * 更新用户角色.
	 * @author 黄祥谦.
	 * @param model
	 * @param roleId
	 */
	void updateUser(UserPo model, Long[] roleId);

	/**
	 * 得到权限对应的url
	 * @author 黄祥谦.
	 * @param userId
	 * @return
	 */
	List<String> getPrivilegeUrl(Long userId);

}
