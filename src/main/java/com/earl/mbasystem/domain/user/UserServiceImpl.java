package com.earl.mbasystem.domain.user;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;
import com.earl.mbasystem.domain.role.RoleDao;
import com.earl.mbasystem.helper.JsonHelper;
import com.earl.mbasystem.security.shiro.ShiroPrincipal;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
 @Service(value = "userService")
public class UserServiceImpl extends BaseServiceImpl<UserPo> implements
		UserService {

	@Resource(name = "userDao")
	UserDao userDao;
	@Resource(name = "roleDao")
	RoleDao roleDao;

	@PostConstruct
	public void initBaseDao(){
		baseDao = userDao;
	}

	@Override
	public List<UserPo> findAllUser() {
		// TODO 未测试.
		List<UserPo> userList = userDao.findAll();
		for (UserPo userPo : userList) {
			List<String> roleList = roleDao.getByUser(userPo.getUserId());
			userPo.setRoleList(JsonHelper.toJson(roleList));
		}
		return userList;
	}

	@Override
	public void updateUser(UserPo model, Long[] roleId) {
		// TODO 未测试.
		userDao.updateUser(model,roleId);
		Subject subject = SecurityUtils.getSubject();  
		ShiroPrincipal user= (ShiroPrincipal) subject.getPrincipal();
		
		List<String> authorities = userDao.getPrivilegeUrl(user.getUser().getUserId());
		List<String> rolelist = userDao.getRolesName(user.getUser().getUserId());
		user.setAuthorities(authorities);
		user.setRoles(rolelist);
	}
	
}
