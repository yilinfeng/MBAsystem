package com.earl.mbasystem.domain.user;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.codec.Hex;
import org.apache.shiro.subject.Subject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.annotation.ReturnValue;
import com.earl.mbasystem.domain.base.BaseAction;
import com.earl.mbasystem.domain.role.RolePo;
import com.earl.mbasystem.security.shiro.ShiroPrincipal;
import com.earl.mbasystem.util.Digests;
import com.earl.mbasystem.vo.DwzVo;

/**
 * 
 * 用途+action 如Demo+Action-->DemoAction
 * 
 * @author Administrator
 * 
 */

@Controller(value = "userAction")
@Scope(value = "prototype")
public class UserAction extends BaseAction<UserPo> {

	/**
	 * 
	 */
	
	UserPo user;
	
	List<UserPo> userList;
	
	List<RolePo> roleList;
	
	Long[] roleId;
	
	public Long[] getRoleId() {
		return roleId;
	}

	public void setRoleId(Long[] roleId) {
		this.roleId = roleId;
	}

	public List<RolePo> getRoleList() {
		return roleList;
	}

	public UserPo getUser() {
		return user;
	}

	public void setUser( UserPo user) {
		this.user = user;
	}

	public void setRoleList(List<RolePo> roleList) {
		this.roleList = roleList;
	}

	public List<UserPo> getUserList() {
		return userList;
	}

	public void setUserList(List<UserPo> userList) {
		this.userList = userList;
	}

	private static final long serialVersionUID = 3293435262298029608L;

	protected Object result;

	@ReturnValue
	// 返回实体对象，或者其他任意对象
	public Object getResultMessage() {
		return result;
	}

	// 下面填写业务逻辑

	/**
	 * 添加对象.
	 * 
	 * @author 黄祥谦.
	 */
	public void addUser() {
		userServer.save(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("添加成功");
		dwzVo.setForwardUrl("user_findAllUser.action");
	}

	public void deleteUser(){
		userServer.deleteById(model.getUserId());
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("删除成功");
		dwzVo.setForwardUrl("user_findAllUser.action");
		result = dwzVo;
	}
	
	public String findAllUser(){
		userList = userServer.findAllUser();
		return "userList";
	}
	
	public String dologin() {
		String username = model.getEmployeeName();
		String password = model.getPassword();
		Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(username,
				password);
		try {
			subject.login(token);
//			resultMessage.setResultInfo("登录成功");
			return "index";
		} catch (UnknownAccountException ue) {
			token.clear();
//			resultMessage = new ResultMessage();
//			resultMessage.setResultInfo("登录失败，您输入的账号不存在");
		} catch (IncorrectCredentialsException ie) {
			ie.printStackTrace();
			token.clear();
//			resultMessage = new ResultMessage();
//			resultMessage.setResultInfo("登录失败，密码不匹配");
		} catch (RuntimeException re) {
			re.printStackTrace();
			token.clear();
//			resultMessage = new ResultMessage();
//			resultMessage.setResultInfo("登录失败");
		}
		return null;
	}
	
	public void isLogin(){
//		Subject subject = SecurityUtils.getSubject();
//		UserPo user = ShiroUtils.getUser();
//		boolean authenticated = subject.isAuthenticated();
//		resultMessage = new ResultMessage();
//		resultMessage.setServiceResult(authenticated);
//		if(authenticated){
//			resultMessage.setResultInfo("已经登录系统");
//		}else{
//			resultMessage.setResultInfo("还没有登录系统");
//		}
//		resultMessage.getResultParm().put("user", user);
	}
	
	public void getRole(){
		ShiroPrincipal subject = (ShiroPrincipal) SecurityUtils.getSubject().getPrincipal();
		List<String> roles = subject.getRoles();
		List<String> authorities = subject.getAuthorities();
//		resultMessage = new ResultMessage();
//		resultMessage.setServiceResult(true);
//		resultMessage.getResultParm().put("roleList", roles);
//		resultMessage.getResultParm().put("authoritieList", authorities);
	}
	
	public void test(){
		
		//密码加密算法
		byte[] sha1 = Digests.sha1("1234".getBytes(),null,1024);//散列次数 1024
		
		String encodeToString = Hex.encodeToString(sha1);
		sha1.toString();
		System.out.println(encodeToString);
//		resultMessage = new ResultMessage();
//		resultMessage.setServiceResult(true);
////		7110eda4d09e062aa5e4a390b0a572ac0d2c0220
////		719a8f29e3a1746fdb1736e9f6f7e63a553737f0
////		719a8f29e3a1746fdb1736e9f6f7e63a553737f0
//		resultMessage.getResultParm().put("encodeToString", encodeToString);
	}
	
	public String userRoleList(){
		
		roleList = roleServer.userRoleList(model.getUserId());
		return "roleList";
	}
	
	public String preUpdate(){
		user = userServer.get(model.getUserId());
		user.setRoleArrayList(roleServer.userRoleList(model.getUserId()));
		roleList = roleServer.findAll();
		return "editUser";
	}
	
	public void updateUser(){
		userServer.updateUser(model,roleId);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("更新成功");
		dwzVo.setForwardUrl("user_findAllUser.action");
		result = dwzVo;
	}
}
