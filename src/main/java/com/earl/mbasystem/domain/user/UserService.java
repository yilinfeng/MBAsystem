package com.earl.mbasystem.domain.user;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseService;

public interface UserService extends BaseService<UserPo>{

	/**
	 * 查找所有user用户.
	 * @author 黄祥谦.
	 * @return
	 */
	List<UserPo> findAllUser();

	/**
	 * 更新用户角色.
	 * @author 黄祥谦.
	 * @param model
	 * @param roleId
	 */
	void updateUser(UserPo model, Long[] roleId);

	

	
	
	
}
