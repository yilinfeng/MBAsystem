package com.earl.mbasystem.domain.department;

import com.earl.mbasystem.annotation.IdAnnotatioin;


public class DepartmentPo{


	/**
	 * 字段描述：Long 
	 * 字段类型：departmentId  
	 */
	@IdAnnotatioin
	private Long departmentId ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：departmentName  
	 */
	private String departmentName ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：describe  
	 */
	private String describe ;
	
	/**
	 * 字段描述：Long 
	 * 字段类型：parentId  
	 */
	private Long parentId ;
	
	
	public void setDepartmentId(Long departmentId){
		this.departmentId = departmentId;
	}
	public Long getDepartmentId() {
		return this.departmentId;
	}
	public void setDepartmentName(String departmentName){
		this.departmentName = departmentName;
	}
	public String getDepartmentName() {
		return this.departmentName;
	}
	public void setDescribe(String describe){
		this.describe = describe;
	}
	public String getDescribe() {
		return this.describe;
	}
	public void setParentId(Long parentId){
		this.parentId = parentId;
	}
	public Long getParentId() {
		return this.parentId;
	}
	@Override
	public String toString() {
		return "DepartmentPo [departmentId=" + departmentId
				+ ", departmentName=" + departmentName + ", describe="
				+ describe + ", parentId=" + parentId + "]";
	}
}
