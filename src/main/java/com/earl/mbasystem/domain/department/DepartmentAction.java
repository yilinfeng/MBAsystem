package com.earl.mbasystem.domain.department;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.annotation.ReturnValue;
import com.earl.mbasystem.domain.base.BaseAction;
import com.earl.mbasystem.vo.DwzVo;

/**
 * 
 * 用途+action 如Demo+Action-->DemoAction
 * 
 * @author Administrator
 * 
 */
@Controller(value = "departmentAction")
@Scope(value = "prototype")
public class DepartmentAction extends BaseAction<DepartmentPo> {

	DepartmentPo department;
	
	List<DepartmentPo> departmentList;
	
	public List<DepartmentPo> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(List<DepartmentPo> departmentList) {
		this.departmentList = departmentList;
	}
	public DepartmentPo getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentPo department) {
		this.department = department;
	}

	private static final long serialVersionUID = 3293435262298029608L;

	protected Object result;

	@ReturnValue //返回实体对象，或者其他任意对象
	public Object getResultMessage() {
		return result;
	}

	// 下面填写业务逻辑

	/**
	 * 添加对象.
	 * @author 黄祥谦.
	 */
	public void addDepartment() {
		departmentServer.save(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("添加成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("department_findAllDepartment.action");
		result = dwzVo;
	}
	
	/**
	 * 查询所有部门.
	 * @author 黄祥谦.
	 * @return
	 */
	public String findAllDepartment(){
		departmentList = departmentServer.findAll();
		return "departmentList";
	}
	
	/**
	 * 删除部门信息.
	 * @author 黄祥谦.
	 */
	public void deleteDepartment(){
		departmentServer.deleteById(model.getDepartmentId());
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("添加成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("department_findAllDepartment.action");
		result = dwzVo;
	}
	
	/**
	 * 预处理添加表单.
	 * @author 黄祥谦.
	 * @return
	 */
	public String preAddDepartment(){
		departmentList = departmentServer.allTopDepartment();
		return "preAddDepartment";
	}
	
	public String preAddTwoDepartment(){
		departmentList = departmentServer.allTopDepartment();
		return "preAddTwoDepartment";
		
	}
	
	public String aDepartment(){
		department = departmentServer.get(model.getDepartmentId());
		departmentList = departmentServer.allTopDepartment();
		return "aDepartment";
	}
	public void updateDepartment(){ 
		departmentServer.updateWithNotNullProperties(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("添加成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("department_findAllDepartment.action");
		result = dwzVo;
	}
	
	public void nextLevelDepartment(){
		List<DepartmentPo> departmentList = departmentServer.nextLevelDepartment(model.getDepartmentId());
		
		List<List<String>> departmentVoList= new ArrayList<List<String>>();
		for (DepartmentPo departmentPo : departmentList) {
			List<String> list = new ArrayList<String>();
			list.add(departmentPo.getDepartmentId().toString());
			list.add(departmentPo.getDepartmentName());
			departmentVoList.add(list);
		}
		result=departmentVoList;
	}
}
