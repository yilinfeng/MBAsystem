package com.earl.mbasystem.domain.department;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.earl.mbasystem.domain.base.BaseDaoImpl;


/**
 * 
 * 
 */
@Repository("departmentDao")
public class DepartmentDaoImpl extends BaseDaoImpl<DepartmentPo> implements DepartmentDao {

	@Override
	public List<DepartmentPo> allTopDepartment() {
		// TODO 未测试.
		String hql ="from DepartmentPo where parentId=null";
		@SuppressWarnings("unchecked")
		List<DepartmentPo> list = getCurrentSession().createQuery(hql).list();
		return list;
	}

	@Override
	public List<DepartmentPo> nextLevelDepartment(Long departmentId) {
		// TODO 未测试.
		String hql ="from DepartmentPo where parentId=:departmentId";
		@SuppressWarnings("unchecked")
		List<DepartmentPo> list = getCurrentSession().createQuery(hql).setLong("departmentId", departmentId).list();
		return list;
	}
	
}