package com.earl.mbasystem.domain.department;


/**
 * @author 黄祥谦.
 * @date:2015-12-24 下午5:03:09
 * @version :
 */
public class DepartmentVo {

	/**
	 * 字段描述：Long 
	 * 字段类型：departmentId  
	 */
	private Long departmentId ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：departmentName  
	 */
	private String departmentName ;

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	@Override
	public String toString() {
		return "DepartmentVo [departmentId=" + departmentId
				+ ", departmentName=" + departmentName + "]";
	}
}
