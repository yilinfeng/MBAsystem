package com.earl.mbasystem.domain.department;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
 @Service(value = "departmentService")
public class DepartmentServiceImpl extends BaseServiceImpl<DepartmentPo> implements
		DepartmentService {

	@Resource(name = "departmentDao")
	DepartmentDao departmentDao;

	@PostConstruct
	public void initBaseDao(){
		baseDao = departmentDao;
	}

	@Override
	public List<DepartmentPo> allTopDepartment() {
		// TODO 未测试.
		List<DepartmentPo> departmentList = departmentDao.allTopDepartment();
		return departmentList;
	}

	@Override
	public List<DepartmentPo> nextLevelDepartment(Long departmentId) {
		// TODO 未测试.
		List<DepartmentPo> departmentList = departmentDao.nextLevelDepartment(departmentId);
		return departmentList;
		
	}
	
}
