package com.earl.mbasystem.domain.department;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseDao;

public interface DepartmentDao extends BaseDao<DepartmentPo>{

	List<DepartmentPo> allTopDepartment();

	/**
	 * 得到下一级部门.
	 * @author 黄祥谦.
	 * @param departmentId
	 * @return
	 */
	List<DepartmentPo> nextLevelDepartment(Long departmentId);

}
