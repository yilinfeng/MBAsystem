package com.earl.mbasystem.domain.department;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseService;

public interface DepartmentService extends BaseService<DepartmentPo>{

	/**
	 * 得到所有顶级的department.
	 * @author 黄祥谦.
	 * @return TODO
	 */
	List<DepartmentPo> allTopDepartment();

	/**
	 * 得到下一级部门. 
	 * @author 黄祥谦.
	 * @param departmentId
	 * @return
	 */
	List<DepartmentPo> nextLevelDepartment(Long departmentId);

	

	
	
	
}
