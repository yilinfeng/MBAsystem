package com.earl.mbasystem.domain.base;

import java.lang.reflect.ParameterizedType;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.interceptor.ApplicationAware;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.domain.department.DepartmentService;
import com.earl.mbasystem.domain.employee.EmployeeService;
import com.earl.mbasystem.domain.position.PositionService;
import com.earl.mbasystem.domain.privilege.PrivilegeService;
import com.earl.mbasystem.domain.role.RoleService;
import com.earl.mbasystem.domain.roleprivilegemapping.RolePrivilegeManageService;
import com.earl.mbasystem.domain.salaryitem.SalaryItemService;
import com.earl.mbasystem.domain.salaryorder.SalaryOrderService;
import com.earl.mbasystem.domain.salaryorderdetail.SalaryOrderDetailService;
import com.earl.mbasystem.domain.salarystander.SalaryStanderService;
import com.earl.mbasystem.domain.user.UserService;
import com.earl.mbasystem.domain.userrolemapping.UserRoleManageService;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

//默认是类简单名称，首字母小写
@Controller(value="baseAction")
@Scope(value="prototype")
public class BaseAction<T> extends ActionSupport implements RequestAware,
		SessionAware, ApplicationAware, ModelDriven<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected T model;

	protected Map<String, Object> request;
	protected Map<String, Object> session;
	protected Map<String, Object> application;

	@Resource
	protected UserService userServer;
	@Resource
	protected DepartmentService departmentServer;
	@Resource
	protected EmployeeService employeeServer;
	@Resource
	protected PositionService positionServer;
	@Resource
	protected PrivilegeService privilegeServer;
	@Resource
	protected RoleService roleServer;
	@Resource
	protected SalaryItemService salaryItemServer;
	@Resource
	protected SalaryOrderService salaryOrderServer;
	@Resource
	protected SalaryOrderDetailService salaryOrderDetailServer;
	@Resource
	protected SalaryStanderService salaryStanderServer;
	@Resource
	protected RolePrivilegeManageService rolePrivilegeManageServer;
	@Resource
	protected UserRoleManageService userRoleManageServer;
	
	
//	protected PageInfo pageInfo = new PageInfo();
//
//	public PageInfo getPageInfo() {
//		return pageInfo;
//	}
//
//	public void setPageInfo(PageInfo pageInfo) {
//		this.pageInfo = pageInfo;
//	}

	@SuppressWarnings("unchecked")
	public BaseAction() {
		ParameterizedType type = (ParameterizedType) this.getClass()
				.getGenericSuperclass();
		@SuppressWarnings("rawtypes")
		Class clazz = (Class) type.getActualTypeArguments()[0];
		try {
			model = (T) clazz.newInstance();
			//TODO 输出model的
//			System.out.println(model);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	@Override
	public T getModel() {
		return model;
	}

	@Override
	public void setApplication(Map<String, Object> application) {
		this.application = application;

	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

}
