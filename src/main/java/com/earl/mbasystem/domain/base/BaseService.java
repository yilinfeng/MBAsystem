package com.earl.mbasystem.domain.base;

import java.util.List;

public interface BaseService<T> {

	void save(T model);

	void update(T t);
	
	Boolean updateWithNotNullProperties(T t);

	void deleteById(Long id);

	List<T> findAll();

//	List<T> pagefindAll(PageInfo pageInfo);

	T get(Long id);
	
	List<T> findByGivenCreteria(T object);
	
//	List<T> findByGivenCreteriaWithPage(T object,PageInfo pageInfo);
}
