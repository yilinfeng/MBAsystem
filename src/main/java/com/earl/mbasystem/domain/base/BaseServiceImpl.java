package com.earl.mbasystem.domain.base;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.user.UserDao;
import com.earl.util.FileUploadImpl;

/**
 * @author Administrator
 * 
 * @param <T>
 */
@Service("baseService")
@SuppressWarnings("unchecked")
@Lazy(value=true)
public class BaseServiceImpl<T> implements BaseService<T> {

	// 不同的T对应不同的Dao
	@SuppressWarnings("rawtypes")
	
	protected BaseDao baseDao;
	
	@Resource
	protected UserDao userDao;
	
	@Resource(name = "fileUpload")
	protected FileUploadImpl fileUpload;
	
	public BaseServiceImpl() {

	}

	@Override
	public void save(T model) {
//		Assert.assertNotNull("baseDao is null", baseDao);
			baseDao.save(model);
	}

	@Override
	public void update(T t) {
			baseDao.update(t);
	}

	@Override
	public void deleteById(Long id) {
			baseDao.deleteById(id);
	}

	@Override
	public T get(Long id) {
		return (T) baseDao.get(id);
	}

	@Override
	public List<T> findAll() {
		return baseDao.findAll();
	}

//	@SuppressWarnings("unchecked")
//	@Override
//	public List<T> pagefindAll(PageInfo pageInfo) {
//
//		return baseDao.pageFindAll(pageInfo);
//	}

	@Override
	public List<T> findByGivenCreteria(T object) {

		return baseDao.findByGivenCriteria(object);
	}

	@Override
	public Boolean updateWithNotNullProperties(T t) {
		try {
			baseDao.updateWithNotNullProperties(t);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

//	@SuppressWarnings("unchecked")
//	@Override
//	public List<T> findByGivenCreteriaWithPage(T object, PageInfo pageInfo) {
//
//		return baseDao.findByGivenCreteriaWithPage(object, pageInfo);
//	}
}
