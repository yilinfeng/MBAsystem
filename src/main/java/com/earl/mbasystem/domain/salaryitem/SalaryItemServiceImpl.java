package com.earl.mbasystem.domain.salaryitem;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
 @Service(value = "salaryItemService")
public class SalaryItemServiceImpl extends BaseServiceImpl<SalaryItemPo> implements
		SalaryItemService {

	@Resource(name = "salaryItemDao")
	SalaryItemDao salaryItemDao;

	@PostConstruct
	public void initBaseDao(){
		baseDao = salaryItemDao;
	}
	
}
