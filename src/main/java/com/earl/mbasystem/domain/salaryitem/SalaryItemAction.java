package com.earl.mbasystem.domain.salaryitem;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.annotation.ReturnValue;
import com.earl.mbasystem.domain.base.BaseAction;
import com.earl.mbasystem.vo.DwzVo;

/**
 * 
 * 用途+action 如Demo+Action-->DemoAction
 * 
 * @author Administrator
 * 
 */
@Controller(value = "salaryItemAction")
@Scope(value = "prototype")
public class SalaryItemAction extends BaseAction<SalaryItemPo> {

	SalaryItemPo salaryItem;
	
	List<SalaryItemPo> salaryItemList;
	 
	private static final long serialVersionUID = 3293435262298029608L;

	protected Object result;

	public SalaryItemPo getSalaryItem() {
		return salaryItem;
	}

	public void setSalaryItem(SalaryItemPo salaryItem) {
		this.salaryItem = salaryItem;
	}

	public List<SalaryItemPo> getSalaryItemList() {
		return salaryItemList;
	}

	public void setSalaryItemList(List<SalaryItemPo> salaryItemList) {
		this.salaryItemList = salaryItemList;
	}

	@ReturnValue //返回实体对象，或者其他任意对象
	public Object getResultMessage() {
		return result;
	}

	// 下面填写业务逻辑

	/**
	 * 添加对象.
	 * @author 黄祥谦.
	 */
	public void addSalaryItem() {
		salaryItemServer.save(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("添加成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("salaryitem_allSalaryItem.action");
		result = dwzVo;
	}
	
	/**
	 * 删除薪酬项目
	 * @author 黄祥谦.
	 */
	public void deleteSalaryItem(){
		salaryItemServer.deleteById(model.getSalaryItemId());
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("添加成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("salaryitem_allSalaryItem.action");
		result = dwzVo;
	}
	
	/**
	 * 更新薪酬项目
	 * @author 黄祥谦.
	 */
	public void updateSalaryItem(){
		salaryItemServer.updateWithNotNullProperties(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("添加成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("salaryitem_allSalaryItem.action");
		result = dwzVo;
	}
	
	/**
	 * 查询所有薪酬项目
	 * @author 黄祥谦.
	 * @return
	 */
	public String allSalaryItem(){
		salaryItemList = salaryItemServer.findAll();
		return "salaryitemList";
	}
	
	/**
	 * 得到指定的薪酬项目信息，更新操作的预处理
	 * @author 黄祥谦.
	 * @return
	 */
	public String aSalaryItem(){
		salaryItem = salaryItemServer.get(model.getSalaryItemId());
		return "aSalaryItem";
	}
}
