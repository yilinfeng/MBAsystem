package com.earl.mbasystem.domain.salaryitem;

import org.springframework.stereotype.Repository;

import com.earl.mbasystem.domain.base.BaseDaoImpl;


/**
 * 
 * 
 */
@Repository("salaryItemDao")
public class SalaryItemDaoImpl extends BaseDaoImpl<SalaryItemPo> implements SalaryItemDao {
	// property constants
	
}