package com.earl.mbasystem.domain.salaryitem;

import com.earl.mbasystem.annotation.IdAnnotatioin;


public class SalaryItemPo{


	/**
	 * 字段描述：Long 
	 * 字段类型：salaryItemId  
	 */
	@IdAnnotatioin
	private Long salaryItemId ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：salaryItemName  
	 */
	private String salaryItemName ;
	
	private String price;
	
	/**
	 * 字段描述：String 
	 * 字段类型：salaryType  
	 */
	private String salaryType;
	
	public void setSalaryItemId(Long salaryItemId){
		this.salaryItemId = salaryItemId;
	}
	public Long getSalaryItemId() {
		return this.salaryItemId;
	}
	public void setSalaryItemName(String salaryItemName){
		this.salaryItemName = salaryItemName;
	}
	public String getSalaryItemName() {
		return this.salaryItemName;
	}
	public void setSalaryType(String salaryType){
		this.salaryType = salaryType;
	}
	public String getSalaryType() {
		return this.salaryType;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "SalaryItemPo [salaryItemId=" + salaryItemId
				+ ", salaryItemName=" + salaryItemName + ", price=" + price
				+ ", salaryType=" + salaryType + "]";
	}
}
