package com.earl.mbasystem.domain.salaryorderdetail;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseService;

public interface SalaryOrderDetailService extends BaseService<SalaryOrderDetailPo>{

	/**
	 * 得到指定订单编号的订单项目.
	 * @author 黄祥谦.
	 * @param salaryOrderId
	 * @return
	 */
	List<SalaryOrderDetailPo> pointOrderDetail(Long salaryOrderId);

	/**
	 * 通过指定订单详情.
	 * @author 黄祥谦.
	 * @param salaryOrderDetailId
	 */
	void recheckOrderDetail(Long salaryOrderDetailId);

	/**
	 * 得到未复核的订单项
	 * @author 黄祥谦.
	 * @param salaryOrderId
	 * @return
	 */
	List<SalaryOrderDetailPo> pointUnRecheckOrderDetail(Long salaryOrderId);

}
