package com.earl.mbasystem.domain.salaryorderdetail;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseDao;

public interface SalaryOrderDetailDao extends BaseDao<SalaryOrderDetailPo>{

	/**
	 * 生成订单明细.
	 * @author 黄祥谦.
	 * @param department3
	 * @return
	 */
	List<SalaryOrderDetailPo> generatOrderDetail(Long department3);

	/**
	 * 指定订单编号的订单详情.
	 * @author 黄祥谦.
	 * @param salaryOrderId
	 * @return
	 */
	List<SalaryOrderDetailPo> pointOrderDetail(Long salaryOrderId);

	/**
	 * 得到未复核订单项目.
	 * @author 黄祥谦.
	 * @param salaryOrderId
	 * @return
	 */
	List<SalaryOrderDetailPo> pointUnRecheckOrderDetail(Long salaryOrderId);

}
