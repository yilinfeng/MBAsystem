package com.earl.mbasystem.domain.salaryorderdetail;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;
import com.earl.mbasystem.domain.salaryorder.SalaryOrderDao;
import com.earl.mbasystem.domain.salaryorder.SalaryOrderPo;
import com.earl.mbasystem.domain.salarystander.SalaryStanderPo;
import com.earl.mbasystem.helper.JsonHelper;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
 @Service(value = "salaryOrderDetailService")
public class SalaryOrderDetailServiceImpl extends BaseServiceImpl<SalaryOrderDetailPo> implements
		SalaryOrderDetailService {

	@Resource(name = "salaryOrderDetailDao")
	SalaryOrderDetailDao salaryOrderDetailDao;
	
	@Resource(name = "salaryOrderDao")
	SalaryOrderDao salaryOrderDao;

	@PostConstruct
	public void initBaseDao(){
		baseDao = salaryOrderDetailDao;
	}

	@Override
	public List<SalaryOrderDetailPo> pointOrderDetail(Long salaryOrderId) {
		// TODO 未测试.
		List<SalaryOrderDetailPo> salaryOrderDetail = salaryOrderDetailDao.pointOrderDetail(salaryOrderId);
		for (SalaryOrderDetailPo salaryOrderDetailPo : salaryOrderDetail) {
			SalaryStanderPo jsonToBean = JsonHelper.jsonToBean(salaryOrderDetailPo.getSalarDetail(), SalaryStanderPo.class);
			salaryOrderDetailPo.setSalaryItemList(jsonToBean.getSalaryArrayList());
		}
		return salaryOrderDetail;
	}

	@Override
	public void recheckOrderDetail(Long salaryOrderDetailId) {
		// TODO 未测试.
		SalaryOrderDetailPo salaryOrderDetailPo = salaryOrderDetailDao.get(salaryOrderDetailId);
		salaryOrderDetailPo.setHaverecheck(true);
		salaryOrderDetailDao.update(salaryOrderDetailPo);
		List<SalaryOrderDetailPo> pointOrderDetail = pointUnRecheckOrderDetail(salaryOrderDetailPo.getSalaryOrderId());
		if(pointOrderDetail.size()==0){
			SalaryOrderPo salaryOrderPo = salaryOrderDao.get(salaryOrderDetailPo.getSalaryOrderId());
			salaryOrderPo.setHaveRecheck(true);
			salaryOrderDao.update(salaryOrderPo);
		}
	}

	@Override
	public List<SalaryOrderDetailPo> pointUnRecheckOrderDetail(
			Long salaryOrderId) {
		// TODO 未测试.
		List<SalaryOrderDetailPo> salaryOrderDetail = salaryOrderDetailDao.pointUnRecheckOrderDetail(salaryOrderId);
		for (SalaryOrderDetailPo salaryOrderDetailPo : salaryOrderDetail) {
			SalaryStanderPo jsonToBean = JsonHelper.jsonToBean(salaryOrderDetailPo.getSalarDetail(), SalaryStanderPo.class);
			salaryOrderDetailPo.setSalaryItemList(jsonToBean.getSalaryArrayList());
		}
		return salaryOrderDetail;
	}
	

	
}
