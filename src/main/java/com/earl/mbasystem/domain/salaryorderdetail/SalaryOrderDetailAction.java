package com.earl.mbasystem.domain.salaryorderdetail;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.annotation.ReturnValue;
import com.earl.mbasystem.domain.base.BaseAction;
import com.earl.mbasystem.vo.DwzVo;

/**
 * 
 * 用途+action 如Demo+Action-->DemoAction
 * 
 * @author Administrator
 * 
 */
@Controller(value = "salaryOrderDetailAction")
@Scope(value = "prototype")
public class SalaryOrderDetailAction extends BaseAction<SalaryOrderDetailPo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3293435262298029608L;

	List<SalaryOrderDetailPo> salaryOrderDetailList;
	
	SalaryOrderDetailPo salaryOrderDetail;
	
	protected Object result;

	public List<SalaryOrderDetailPo> getSalaryOrderDetailList() {
		return salaryOrderDetailList;
	}

	public void setSalaryOrderDetailList(
			List<SalaryOrderDetailPo> salaryOrderDetailList) {
		this.salaryOrderDetailList = salaryOrderDetailList;
	}

	public SalaryOrderDetailPo getSalaryOrderDetail() {
		return salaryOrderDetail;
	}

	public void setSalaryOrderDetail(SalaryOrderDetailPo salaryOrderDetail) {
		this.salaryOrderDetail = salaryOrderDetail;
	}

	@ReturnValue //返回实体对象，或者其他任意对象
	public Object getResultMessage() {
		return result;
	}

	// 下面填写业务逻辑

	/**
	 * 更新指定订单项目.
	 * @author 黄祥谦.
	 */
	public void updateSalaryOrderDetail(){
		salaryOrderDetailServer.updateWithNotNullProperties(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setMessage("更新成功");
		dwzVo.setStatusCode("200");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("salaryorder_allcheckOrder.action");
		result=dwzVo;
	}
	
	/**
	 * 复查薪酬订单项目.
	 * @author 黄祥谦.
	 */
	public void recheckOrderDetail(){
		salaryOrderDetailServer.recheckOrderDetail(model.getSalaryOrderDetailId());
		DwzVo dwzVo = new DwzVo();
		dwzVo.setMessage("复核成功");
		dwzVo.setStatusCode("200");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("salaryorder_allcheckOrder.action");
		result=dwzVo;
	}
	
	/**
	 * 指定订单下的订单项目（不包括已经复查的）.
	 * @author 黄祥谦.
	 */
	public String pointOrderDetail(){
		salaryOrderDetailList = salaryOrderDetailServer.pointUnRecheckOrderDetail(model.getSalaryOrderId());
		return "salaryOrderDetailList";
	}
	
	/**
	 * 得到指定订单下的订单项目（所有订单项目，包括复核过的和未经过复查的）
	 * @author 黄祥谦.
	 * @return
	 */
	public String allPointOrderDetail(){
		salaryOrderDetailList = salaryOrderDetailServer.pointOrderDetail(model.getSalaryOrderId());
		return "salaryOrderDetailList";
	}
	
	/**
	 * 预处理薪酬订单项，更新预处理.
	 * @author 黄祥谦.
	 * @return
	 */
	public String aSalaryOrderDetail(){
		salaryOrderDetail = salaryOrderDetailServer.get(model.getSalaryOrderDetailId());
		return "aSalaryOrderDetail";
	}
}
