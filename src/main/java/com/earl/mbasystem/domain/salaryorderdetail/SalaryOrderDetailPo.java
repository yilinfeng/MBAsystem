package com.earl.mbasystem.domain.salaryorderdetail;

import java.util.List;

import com.earl.mbasystem.annotation.IdAnnotatioin;
import com.earl.mbasystem.domain.salaryitem.SalaryItemPo;


public class SalaryOrderDetailPo{


	@IdAnnotatioin
	private Long salaryOrderDetailId;
	/**
	 * 字段描述：Long 
	 * 字段类型：privilegeId  
	 */
	private Long salaryOrderId;
	
	private Long employeeId;
	
	private String employeeName;
	
	/**
	 * 加钱
	 */
	private Double prase;
	
	/**
	 * 扣钱
	 */
	private Double decs;
	
	private String salarDetail;
	
	public Boolean haverecheck;
	
	private List<SalaryItemPo> salaryItemList;
	
	public Boolean getHaverecheck() {
		return haverecheck;
	}
	public void setHaverecheck(Boolean haverecheck) {
		this.haverecheck = haverecheck;
	}
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public Double getPrase() {
		return prase;
	}
	public void setPrase(Double prase) {
		this.prase = prase;
	}
	public Double getDecs() {
		return decs;
	}
	public void setDecs(Double decs) {
		this.decs = decs;
	}
	public Long getSalaryOrderDetailId() {
		return salaryOrderDetailId;
	}
	public List<SalaryItemPo> getSalaryItemList() {
		return salaryItemList;
	}
	public void setSalaryItemList(List<SalaryItemPo> salaryItemList) {
		this.salaryItemList = salaryItemList;
	}
	public void setSalaryOrderDetailId(Long salaryOrderDetailId) {
		this.salaryOrderDetailId = salaryOrderDetailId;
	}
	public Long getSalaryOrderId() {
		return salaryOrderId;
	}
	public void setSalaryOrderId(Long salaryOrderId) {
		this.salaryOrderId = salaryOrderId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getSalarDetail() {
		return salarDetail;
	}
	public void setSalarDetail(String salarDetail) {
		this.salarDetail = salarDetail;
	}
}
