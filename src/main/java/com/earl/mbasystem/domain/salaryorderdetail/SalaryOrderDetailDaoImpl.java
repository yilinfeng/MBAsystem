package com.earl.mbasystem.domain.salaryorderdetail;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.earl.mbasystem.domain.base.BaseDaoImpl;
import com.earl.mbasystem.domain.salarystander.SalaryStanderPo;
import com.earl.mbasystem.helper.JsonHelper;


/**
 * 
 * 
 */
@Repository("salaryOrderDetailDao")
public class SalaryOrderDetailDaoImpl extends BaseDaoImpl<SalaryOrderDetailPo> implements SalaryOrderDetailDao {

	@Override
	public List<SalaryOrderDetailPo> generatOrderDetail(Long department3) {
		// TODO 未测试.
		String sql = "select e.employeeId,e.employeeName,s.salaryItemList from employee e "+ 
				"left outer join salarystander s on e.salarystandard=s.salarystanderId  "+
				"where e.havarecheck=true and e.isDelete=false and e.department3=:department3 ";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = getCurrentSession().createSQLQuery(sql).setLong("department3", department3).list();
		ArrayList<SalaryOrderDetailPo> arrayList = new ArrayList<SalaryOrderDetailPo>();
		for (Object[] objects : list) {
			System.out.println(objects[0]);
			SalaryOrderDetailPo salaryOrderDetail = new SalaryOrderDetailPo();
			salaryOrderDetail.setEmployeeId(((BigInteger)objects[0]).longValue());
			salaryOrderDetail.setEmployeeName((objects[1]).toString());
			salaryOrderDetail.setSalarDetail((objects[2]).toString());
			
			SalaryStanderPo jsonToBean = JsonHelper.jsonToBean(salaryOrderDetail.getSalarDetail(), SalaryStanderPo.class);
			salaryOrderDetail.setSalaryItemList(jsonToBean.getSalaryArrayList());
			salaryOrderDetail.setPrase(0.0);
			salaryOrderDetail.setDecs(0.0);
			salaryOrderDetail.setHaverecheck(false);
			arrayList.add(salaryOrderDetail);
		}
		return arrayList;
	}
	// property constants

	@Override
	public List<SalaryOrderDetailPo> pointOrderDetail(Long salaryOrderId) {
		// TODO 未测试.
		String hql ="from SalaryOrderDetailPo where salaryOrderId=:salaryOrderId";
		@SuppressWarnings("unchecked")
		List<SalaryOrderDetailPo> list = getCurrentSession().createQuery(hql).setLong("salaryOrderId", salaryOrderId).list();
		return list;
	}

	@Override
	public List<SalaryOrderDetailPo> pointUnRecheckOrderDetail(
			Long salaryOrderId) {
		// TODO 未测试.
		String hql ="from SalaryOrderDetailPo where salaryOrderId=:salaryOrderId and haverecheck=false";
		@SuppressWarnings("unchecked")
		List<SalaryOrderDetailPo> list = getCurrentSession().createQuery(hql).setLong("salaryOrderId", salaryOrderId).list();
		return list;
	}

	
}