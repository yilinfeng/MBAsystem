package com.earl.mbasystem.domain.salarystander;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;
import com.earl.mbasystem.domain.employee.DateQuery;
import com.earl.mbasystem.domain.salaryitem.SalaryItemDao;
import com.earl.mbasystem.domain.salaryitem.SalaryItemPo;
import com.earl.mbasystem.helper.JsonHelper;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
@Service(value = "salaryStanderService")
public class SalaryStanderServiceImpl extends BaseServiceImpl<SalaryStanderPo>
		implements SalaryStanderService {

	public List<SalaryItemPo> commonSalaryItem = new ArrayList<SalaryItemPo>();

	static Long jiben = 1L;
	static Long yanglao = 5L;
	static Long shiye = 6L;
	static Long yiliao = 7L;
	static Long zhufang = 8L;

	@Resource(name = "salaryStanderDao")
	SalaryStanderDao salaryStanderDao;

	@Resource(name = "salaryItemDao")
	SalaryItemDao salaryItemDao;

	@PostConstruct
	public void initBaseDao() {
		baseDao = salaryStanderDao;
		commonSalaryItem.add(salaryItemDao.get(yanglao));// 养老保险
		commonSalaryItem.add(salaryItemDao.get(shiye));// 失业保险
		commonSalaryItem.add(salaryItemDao.get(yiliao));// 医疗保险
		commonSalaryItem.add(salaryItemDao.get(zhufang));// 住房公积金

	}

	@Override
	public void addSalaryStander(SalaryStanderPo model) {
		// TODO 未测试.
		SalaryItemPo yanglao1 = salaryItemDao.get(yanglao);
		SalaryItemPo shiye1 = salaryItemDao.get(shiye);
		SalaryItemPo yiliao1 = salaryItemDao.get(yiliao);
		SalaryItemPo zhufang1 = salaryItemDao.get(zhufang);
		SalaryItemPo salaryItemPo = model.getSalaryArrayList().get(0);
		String price = salaryItemPo.getPrice();
		Double price2 = Double.valueOf(price);

		yanglao1.setPrice(String.valueOf((price2 * 0.08)));
		yiliao1.setPrice(String.valueOf((price2 * 0.02 + 3)));
		shiye1.setPrice(String.valueOf((price2 * 0.05)));
		zhufang1.setPrice(String.valueOf((price2 * 0.08)));
		List<SalaryItemPo> salaryArrayList = model.getSalaryArrayList();
		ArrayList<SalaryItemPo> arrayList = new ArrayList<SalaryItemPo>();
		for (SalaryItemPo salaryItemPo2 : salaryArrayList) {
			if (salaryItemPo2.getSalaryItemId() == yanglao
					|| salaryItemPo2.getSalaryItemId() == shiye
					|| salaryItemPo2.getSalaryItemId() == yiliao
					|| salaryItemPo2.getSalaryItemId() == zhufang) {
				continue;
			}
			arrayList.add(salaryItemPo2);
		}

		arrayList.add(yanglao1);
		arrayList.add(yiliao1);
		arrayList.add(shiye1);
		arrayList.add(zhufang1);
		model.setSalaryArrayList(arrayList);
		model.setHavaRecheck(false);
		Double tmpprice = 0.0;
		for (SalaryItemPo salaryItemPo2 : arrayList) {
			String price3 = salaryItemPo2.getPrice();
			Double price4 = Double.valueOf(price3);
			tmpprice += price4;
		}
		model.setPrice(tmpprice);
		model.setSalaryItemList(JsonHelper.toJson(model));
		salaryStanderDao.save(model);
	}

	@Override
	public void updateSalaryStander(SalaryStanderPo model) {
		// TODO 未测试.
		SalaryItemPo yanglao1 = salaryItemDao.get(yanglao);
		SalaryItemPo shiye1 = salaryItemDao.get(shiye);
		SalaryItemPo yiliao1 = salaryItemDao.get(yiliao);
		SalaryItemPo zhufang1 = salaryItemDao.get(zhufang);
		SalaryItemPo salaryItemPo = model.getSalaryArrayList().get(0);
		String price = salaryItemPo.getPrice();
		Double price2 = Double.valueOf(price);

		yanglao1.setPrice(String.valueOf((price2 * 0.08)));
		yiliao1.setPrice(String.valueOf((price2 * 0.02 + 3)));
		shiye1.setPrice(String.valueOf((price2 * 0.05)));
		zhufang1.setPrice(String.valueOf((price2 * 0.08)));
		List<SalaryItemPo> salaryArrayList = model.getSalaryArrayList();
		ArrayList<SalaryItemPo> arrayList = new ArrayList<SalaryItemPo>();
		for (SalaryItemPo salaryItemPo2 : salaryArrayList) {
			if (salaryItemPo2.getSalaryItemId() == yanglao
					|| salaryItemPo2.getSalaryItemId() == shiye
					|| salaryItemPo2.getSalaryItemId() == yiliao
					|| salaryItemPo2.getSalaryItemId() == zhufang) {
				continue;
			}
			arrayList.add(salaryItemPo2);
		}

		arrayList.add(yanglao1);
		arrayList.add(yiliao1);
		arrayList.add(shiye1);
		arrayList.add(zhufang1);
		model.setSalaryArrayList(arrayList);

		Double tmpprice = 0.0;
		for (SalaryItemPo salaryItemPo2 : arrayList) {
			String price3 = salaryItemPo2.getPrice();
			Double price4 = Double.valueOf(price3);
			tmpprice += price4;
		}
		model.setPrice(tmpprice);
		model.setSalaryItemList(JsonHelper.toJson(model));
		model.setHavaRecheck(false);
		salaryStanderDao.update(model);
	}

	@Override
	public List<SalaryStanderPo> allSalaryStander() {
		// TODO 未测试.
		List<SalaryStanderPo> findAll = salaryStanderDao.allSalaryStander();
		for (SalaryStanderPo salaryStanderPo : findAll) {
			@SuppressWarnings("unchecked")
			List<SalaryItemPo> jsonToBean = JsonHelper.jsonToBean(
					salaryStanderPo.getSalaryItemList(), List.class);
			salaryStanderPo.setSalaryArrayList(jsonToBean);
		}
		return findAll;
	}

	@Override
	public List<SalaryStanderPo> allUncheckSalaryStander() {
		// TODO 未测试.
		List<SalaryStanderPo> findAll = salaryStanderDao
				.allUncheckSalaryStander();
		for (SalaryStanderPo salaryStanderPo : findAll) {
			@SuppressWarnings("unchecked")
			List<SalaryItemPo> jsonToBean = JsonHelper.jsonToBean(
					salaryStanderPo.getSalaryItemList(), List.class);
			salaryStanderPo.setSalaryArrayList(jsonToBean);
		}
		return findAll;
	}

	@Override
	public List<SalaryStanderPo> select(Long salaryStanderId,
			String salaryStanderName, Date begdate, Date enddate) {
		// TODO 未测试.

		DateQuery dateQuery = DateQuery.getDateQuery(begdate, enddate);
		List<SalaryStanderPo> salaryStanderList;
		if (salaryStanderName.equals("")) {
			salaryStanderName = null;
		}
		if (dateQuery == null) {
			salaryStanderList = salaryStanderDao.select(salaryStanderId,
					salaryStanderName, "salaryStanderName");
			salaryStanderList.addAll(salaryStanderDao.select(salaryStanderId,
					salaryStanderName, "planner"));
			salaryStanderList.addAll(salaryStanderDao.select(salaryStanderId,
					salaryStanderName, "operater"));

		} else {
			salaryStanderList = salaryStanderDao.select(salaryStanderId,
					salaryStanderName, "salaryStanderName", dateQuery);
			salaryStanderList.addAll(salaryStanderDao.select(salaryStanderId,
					salaryStanderName, "planner", dateQuery));
			salaryStanderList.addAll(salaryStanderDao.select(salaryStanderId,
					salaryStanderName, "operater", dateQuery));
		}
		List<SalaryStanderPo> removeDuplicate = removeDuplicate(salaryStanderList);
		return removeDuplicate;
	}

	private List<SalaryStanderPo> removeDuplicate(List<SalaryStanderPo> list) {
		Set<SalaryStanderPo> set = new HashSet<SalaryStanderPo>();
		List<SalaryStanderPo> newList = new ArrayList<SalaryStanderPo>();
		for (Iterator<SalaryStanderPo> iter = list.iterator(); iter.hasNext();) {
			SalaryStanderPo element = (SalaryStanderPo) iter.next();
			if (set.add(element))
				newList.add(element);
		}
		return newList;
	}

}
