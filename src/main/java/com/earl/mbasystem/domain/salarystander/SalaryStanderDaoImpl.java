package com.earl.mbasystem.domain.salarystander;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.earl.mbasystem.domain.base.BaseDaoImpl;
import com.earl.mbasystem.domain.employee.DateQuery;


/**
 * 
 * 
 */
@Repository("salaryStanderDao")
public class SalaryStanderDaoImpl extends BaseDaoImpl<SalaryStanderPo> implements SalaryStanderDao {

	@Override
	public List<SalaryStanderPo> allSalaryStander() {
		// TODO 未测试.
		String hql="from SalaryStanderPo where havaRecheck = true";
		@SuppressWarnings("unchecked")
		List<SalaryStanderPo> list = getCurrentSession().createQuery(hql).list();
		return list;
	}
	// property constants
	@Override
	public List<SalaryStanderPo> allUncheckSalaryStander() {
		// TODO 未测试.
		String hql="from SalaryStanderPo where havaRecheck = false";
		@SuppressWarnings("unchecked")
		List<SalaryStanderPo> list = getCurrentSession().createQuery(hql).list();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SalaryStanderPo> select(Long salaryStanderId,
			String salaryStanderName, String string) {
		Criteria ca = getCurrentSession().createCriteria(SalaryStanderPo.class);
		if(salaryStanderId!=null){
			ca.add( Restrictions.eq("salaryStanderId", salaryStanderId));
		}
		if(salaryStanderName!=null){
			ca.add( Restrictions.like(string, salaryStanderName,MatchMode.ANYWHERE));
		}
		return ca.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SalaryStanderPo> select(Long salaryStanderId,
			String salaryStanderName, String string, DateQuery dateObject) {
		Criteria ca = getCurrentSession().createCriteria(SalaryStanderPo.class);
		if(salaryStanderId!=null){
			ca.add( Restrictions.eq("salaryStanderId", salaryStanderId));
		}
		if(salaryStanderName!=null){
			ca.add( Restrictions.like(string, salaryStanderName,MatchMode.ANYWHERE));
		}
		ca.add( Restrictions.between("registerTime", dateObject.begdate, dateObject.enddate)); 
		return ca.list();
	}}