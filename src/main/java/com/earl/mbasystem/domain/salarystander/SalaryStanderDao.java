package com.earl.mbasystem.domain.salarystander;

import java.util.List;

import com.earl.mbasystem.domain.base.BaseDao;
import com.earl.mbasystem.domain.employee.DateQuery;

public interface SalaryStanderDao extends BaseDao<SalaryStanderPo>{

	/**
	 * 已经符合的薪酬标准.
	 * @author 黄祥谦.
	 * @return
	 */
	List<SalaryStanderPo> allSalaryStander();

	List<SalaryStanderPo> allUncheckSalaryStander();

	/**
	 * 查询.
	 * @author 黄祥谦.
	 * @param salaryStanderId
	 * @param salaryStanderName
	 * @param string
	 * @return
	 */
	List<SalaryStanderPo> select(Long salaryStanderId,
			String salaryStanderName, String string);

	List<SalaryStanderPo> select(Long salaryStanderId,
			String salaryStanderName, String string, DateQuery dateQuery);

}
