package com.earl.mbasystem.domain.salarystander;

import java.util.Date;
import java.util.List;

import com.earl.mbasystem.domain.salaryitem.SalaryItemPo;

public class SalaryStanderPo{


	/**
	 * 字段描述：Long 
	 * 字段类型：salaryStanderId  
	 */
	private Long salaryStanderId ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：salaryStanderName  
	 */
	private String salaryStanderName ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：planner  
	 */
	private String planner ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：operater  
	 */
	private String operater ;
	
	/**
	 * 字段描述：Date 
	 * 字段类型：registerTime  
	 */
	private Date registerTime ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：salaryItemList  
	 */
	private String salaryItemList ;
	
	private List<SalaryItemPo> salaryArrayList;
	
	/**
	 * 字段描述：Double 
	 * 字段类型：price  
	 */
	private Double price ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：rechecker  
	 */
	private String rechecker ;
	
	/**
	 * 字段描述：String 
	 * 字段类型：recheckOpinion  
	 */
	private String recheckOpinion ;
	
	/**
	 * 字段描述：Boolean 
	 * 字段类型：havaRecheck  
	 */
	private Boolean havaRecheck ;
	
	public List<SalaryItemPo> getSalaryArrayList() {
		return salaryArrayList;
	}
	public void setSalaryArrayList(List<SalaryItemPo> salaryArrayList) {
		this.salaryArrayList = salaryArrayList;
	}
	public void setSalaryStanderId(Long salaryStanderId){
		this.salaryStanderId = salaryStanderId;
	}
	public Long getSalaryStanderId() {
		return this.salaryStanderId;
	}
	public void setSalaryStanderName(String salaryStanderName){
		this.salaryStanderName = salaryStanderName;
	}
	public String getSalaryStanderName() {
		return this.salaryStanderName;
	}
	public void setPlanner(String planner){
		this.planner = planner;
	}
	public String getPlanner() {
		return this.planner;
	}
	public void setOperater(String operater){
		this.operater = operater;
	}
	public String getOperater() {
		return this.operater;
	}
	public void setRegisterTime(Date registerTime){
		this.registerTime = registerTime;
	}
	public Date getRegisterTime() {
		return this.registerTime;
	}
	public void setSalaryItemList(String salaryItemList){
		this.salaryItemList = salaryItemList;
	}
	public String getSalaryItemList() {
		return this.salaryItemList;
	}
	public void setPrice(Double price){
		this.price = price;
	}
	public Double getPrice() {
		return this.price;
	}
	public void setRechecker(String rechecker){
		this.rechecker = rechecker;
	}
	public String getRechecker() {
		return this.rechecker;
	}
	public void setRecheckOpinion(String recheckOpinion){
		this.recheckOpinion = recheckOpinion;
	}
	public String getRecheckOpinion() {
		return this.recheckOpinion;
	}
	public void setHavaRecheck(Boolean havaRecheck){
		this.havaRecheck = havaRecheck;
	}
	public Boolean getHavaRecheck() {
		return this.havaRecheck;
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO 未测试.
		if(obj ==null){
			return false;
		}
		if(this == obj){
			return true;
		}
		SalaryStanderPo ssp = (SalaryStanderPo)obj;
		if(ssp.getSalaryStanderId() == this.salaryStanderId){
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		// TODO 未测试.
		 int result = 17;  //任意素数   
		 result = 31*result +salaryStanderId.intValue(); //c1,c2是什么看下文解释   
		  return result;   
	}
	
	@Override
	public String toString() {
		return "SalaryStanderPo [salaryStanderId=" + salaryStanderId
				+ ", salaryStanderName=" + salaryStanderName + ", planner="
				+ planner + ", operater=" + operater + ", registerTime="
				+ registerTime + ", salaryItemList=" + salaryItemList
				+ ", salaryArrayList=" + salaryArrayList + ", price=" + price
				+ ", rechecker=" + rechecker + ", recheckOpinion="
				+ recheckOpinion + ", havaRecheck=" + havaRecheck + "]";
	}
}
