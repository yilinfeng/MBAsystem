package com.earl.mbasystem.domain.salarystander;

import java.util.Date;
import java.util.List;

import com.earl.mbasystem.domain.base.BaseService;

public interface SalaryStanderService extends BaseService<SalaryStanderPo>{

	/**
	 * 添加薪酬标准.
	 * @author 黄祥谦.
	 * @param model
	 */
	void addSalaryStander(SalaryStanderPo model);
	
	/**
	 * 更新薪酬模板信息.
	 * @author 黄祥谦.
	 * @param model
	 */
	void updateSalaryStander(SalaryStanderPo model);

	/**
	 * 查询所有的薪酬项目.
	 * @author 黄祥谦.
	 * @return
	 */
	List<SalaryStanderPo> allSalaryStander();

	List<SalaryStanderPo> allUncheckSalaryStander();

	List<SalaryStanderPo> select(Long salaryStanderId,
			String salaryStanderName, Date begdate, Date enddate);


}
