package com.earl.mbasystem.domain.salarystander;

import java.util.Date;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.earl.mbasystem.annotation.ReturnValue;
import com.earl.mbasystem.domain.base.BaseAction;
import com.earl.mbasystem.domain.salaryitem.SalaryItemPo;
import com.earl.mbasystem.domain.user.UserPo;
import com.earl.mbasystem.helper.JsonHelper;
import com.earl.mbasystem.security.shiro.ShiroPrincipal;
import com.earl.mbasystem.vo.DwzVo;

/**
 * 
 * 用途+action 如Demo+Action-->DemoAction
 * 
 * @author Administrator
 * 
 */
@Controller(value = "salaryStanderAction")
@Scope(value = "prototype")
public class SalaryStanderAction extends BaseAction<SalaryStanderPo> {

	/**
	 * 
	 */
	SalaryStanderPo salaryStander;
	
	List<SalaryItemPo> salaryitemList;
	
	List<SalaryStanderPo> salaryStanderList;
	
	UserPo currentUser;
	
	private static final long serialVersionUID = 3293435262298029608L;

	protected Object result;

	private Date begdate;

	private Date enddate;

	public List<SalaryItemPo> getSalaryitemList() {
		return salaryitemList;
	}

	public UserPo getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(UserPo currentUser) {
		this.currentUser = currentUser;
	}

	public SalaryStanderPo getSalaryStander() {
		return salaryStander;
	}

	public void setSalaryStander(SalaryStanderPo salaryStander) {
		this.salaryStander = salaryStander;
	}

	public List<SalaryStanderPo> getSalaryStanderList() {
		return salaryStanderList;
	}

	public void setSalaryStanderList(List<SalaryStanderPo> salaryStanderList) {
		this.salaryStanderList = salaryStanderList;
	}

	public void setSalaryitemList(List<SalaryItemPo> salaryitemList) {
		this.salaryitemList = salaryitemList;
	}

	public Object getBegdate() {
		return begdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public void setBegdate(Date begdate) {
		this.begdate = begdate;
	}

	@ReturnValue //返回实体对象，或者其他任意对象
	public Object getResultMessage() {
		return result;
	}

	// 下面填写业务逻辑

	/**
	 * 添加对象.
	 * @author 黄祥谦.
	 */
	public void addSalaryStander() {
		salaryStanderServer.addSalaryStander(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("添加成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("salarystander_allSalaryStander.action");
		result = dwzVo;
	}
	
	public void updateSalaryStander(){
		salaryStanderServer.updateSalaryStander(model);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("更新成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("salarystander_allSalaryStander.action");
		result = dwzVo;

	}
	
	/**
	 * 预处理薪酬标准.
	 * @author 黄祥谦.
	 * @return
	 */
	public String preAddSalaryStander(){
		salaryitemList = salaryItemServer.findAll();
		ShiroPrincipal principal = (ShiroPrincipal) SecurityUtils.getSubject().getPrincipal();  
		currentUser = principal.getUser();
		return "preAddSalaryStander";
	}
	
	public String allSalaryStander(){
		salaryStanderList = salaryStanderServer.allSalaryStander();
		return "salaryStanderList";
	}
	public String allUnRecheckSalaryStander(){
		salaryStanderList = salaryStanderServer.allUncheckSalaryStander();
		return "unrechecksalaryStanderList";
	}
	
	public void recheckPass(){
		SalaryStanderPo salaryStanderPo = salaryStanderServer.get(model.getSalaryStanderId());
		salaryStanderPo.setHavaRecheck(true);
		salaryStanderServer.update(salaryStanderPo);
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("复核成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("salarystander_allSalaryStander.action");
		result = dwzVo;

	}
	
	public String aSalaryStander(){
		salaryitemList = salaryItemServer.findAll();
		salaryStander = salaryStanderServer.get(model.getSalaryStanderId());
		SalaryStanderPo jsonToBean = JsonHelper.jsonToBean(salaryStander.getSalaryItemList(), SalaryStanderPo.class);
		salaryStander.setSalaryArrayList(jsonToBean.getSalaryArrayList());
		return "aSalaryStander";
	}
	
	public void deleteSalaryStander(){
		salaryStanderServer.deleteById(model.getSalaryStanderId());
		DwzVo dwzVo = new DwzVo();
		dwzVo.setStatusCode("200");
		dwzVo.setMessage("删除成功");
		dwzVo.setCallbackType("forward");//转发
		dwzVo.setForwardUrl("salarystander_allSalaryStander.action");
		result = dwzVo;
	}
	
	public String select(){
		salaryStanderList = salaryStanderServer.select(model.getSalaryStanderId(),model.getSalaryStanderName(),begdate,enddate);
		return "salaryStanderList";
	}
}
