package com.earl.mbasystem.domain.roleprivilegemapping;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.earl.mbasystem.domain.base.BaseServiceImpl;

/**
 * 每个ServiceImpl都要继承相对应的service接口
 * 
 * @author Administrator
 * 
 */
 @Service(value = "rolePrivilegeManageService")
public class RolePrivilegeManageServiceImpl extends BaseServiceImpl<RolePrivilegeManagePo> implements
		RolePrivilegeManageService {

	@Resource(name = "rolePrivilegeManageDao")
	RolePrivilegeManageDao rolePrivilegeManageDao;

	@PostConstruct
	public void initBaseDao(){
		baseDao = rolePrivilegeManageDao;
	}
	
}
