package com.earl.mbasystem.domain.roleprivilegemapping;

import org.springframework.stereotype.Repository;

import com.earl.mbasystem.domain.base.BaseDaoImpl;


/**
 * 
 * 
 */
@Repository("rolePrivilegeManageDao")
public class RolePrivilegeManageDaoImpl extends BaseDaoImpl<RolePrivilegeManagePo> implements RolePrivilegeManageDao {
	// property constants
	
}