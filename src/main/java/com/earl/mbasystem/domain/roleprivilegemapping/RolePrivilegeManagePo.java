package com.earl.mbasystem.domain.roleprivilegemapping;


public class RolePrivilegeManagePo{


	private Long rolePrivilegeManageId;
	/**
	 * 字段描述：Long 
	 * 字段类型：roleId  
	 */
	private Long roleId ;
	
	/**
	 * 字段描述：Long 
	 * 字段类型：privilegeId  
	 */
	private Long privilegeId ;
	
	public void setRoleId(Long roleId){
		this.roleId = roleId;
	}
	public Long getRolePrivilegeManageId() {
		return rolePrivilegeManageId;
	}
	public void setRolePrivilegeManageId(Long rolePrivilegeManageId) {
		this.rolePrivilegeManageId = rolePrivilegeManageId;
	}
	public Long getRoleId() {
		return this.roleId;
	}
	public void setPrivilegeId(Long privilegeId){
		this.privilegeId = privilegeId;
	}
	public Long getPrivilegeId() {
		return this.privilegeId;
	}
}
