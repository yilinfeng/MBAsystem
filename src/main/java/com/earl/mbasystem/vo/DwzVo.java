package com.earl.mbasystem.vo;

public class DwzVo{
	
	private String statusCode;

	private String message ;
	
	private String navTabId;
	
	private String callbackType;
	
	private String forwardUrl;

	
//	 "callbackType":"closeCurrent", //callbackType如果是closeCurrent就会关闭当前tab
	 // 只有callbackType="forward"时需要forwardUrl值
//	 "forwardUrl":""

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getNavTabId() {
		return navTabId;
	}

	public void setNavTabId(String navTabId) {
		this.navTabId = navTabId;
	}

	public String getCallbackType() {
		return callbackType;
	}

	public void setCallbackType(String callbackType) {
		this.callbackType = callbackType;
	}

	public String getForwardUrl() {
		return forwardUrl;
	}

	public void setForwardUrl(String forwardUrl) {
		this.forwardUrl = forwardUrl;
	}

	@Override
	public String toString() {
		return "DwzVo [statusCode=" + statusCode + ", message=" + message
				+ ", navTabId=" + navTabId + ", callbackType=" + callbackType
				+ ", forwardUrl=" + forwardUrl + "]";
	}
}