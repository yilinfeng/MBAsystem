package com.earl.mbasystem.listener;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.earl.util.PropertiesWatch;

public class ReloadPropertiesServletContextListener implements
		ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {

		List<String> filepath = new ArrayList<String>();
		
		filepath.add(this.getClass()
				.getResource("/applicationContext-bean.xml").getPath());
		filepath.add(this.getClass()
				.getResource("/applicationContext-database.xml").getPath());
		filepath.add(this.getClass()
				.getResource("/hibernate.cfg.xml").getPath());
		filepath.add(this.getClass()
				.getResource("/public.properties").getPath());
		
		new PropertiesWatch(filepath,
				event.getServletContext()).start();
	}
}
