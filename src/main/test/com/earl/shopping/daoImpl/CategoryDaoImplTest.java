package com.earl.shopping.daoImpl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.internal.SessionFactoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.earl.fishshop.domain.category.CategoryDao;
import com.earl.fishshop.domain.category.CategoryPo;
import com.earl.mbasystem.vo.PageInfo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath*:applicationContext-*.xml")
public class CategoryDaoImplTest {

	@Resource
	CategoryDao categoryDao;
	
	@Resource(name="sessionFactory")
	SessionFactoryImpl sessionFactory;
	
	@Resource(name="transactionManager")
	HibernateTransactionManager transactionManager;
	
	@Test
	public void testGet() {
		CategoryPo category = categoryDao.get(1L);
		System.out.println(category);
	}
	
}
